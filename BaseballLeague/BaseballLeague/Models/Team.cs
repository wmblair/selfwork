﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseballLeague.Models
{
    public class Team
    {
        public int TeamId { get; set; }
        public int LeagueId { get; set; }
        public string TeamName { get; set; }
        public string Manager { get; set; }

        public List<SelectListItem> TeamItems { get; set; }
        public List<int> SelectedTeamIds { get; set; }

        public Team()
        {
            TeamItems = new List<SelectListItem>();
            SelectedTeamIds = new List<int>();
        }

        public void SetTeamItems(IEnumerable<Team> teams)
        {
            foreach (var team in teams)
            {
                TeamItems.Add(new SelectListItem()
                {
                    Value = team.TeamId.ToString(),
                    Text = team.TeamName
                });
            }
        }
    }
}