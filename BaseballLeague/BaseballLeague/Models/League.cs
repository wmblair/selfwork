﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseballLeague.Models
{
    public class League
    {
        public int LeagueId { get; set; }
        public string LeagueName { get; set; }
    }
}