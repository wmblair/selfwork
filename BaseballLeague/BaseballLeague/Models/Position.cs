﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseballLeague.Models
{
    public class Position
    {
        public int PositionId { get; set; }
        public string PositionName { get; set; }

        public List<SelectListItem> PositionItems { get; set; }
        public List<int> SelectedPositionIds { get; set; }

        public Position()
        {
            PositionItems = new List<SelectListItem>();
            SelectedPositionIds = new List<int>();
        }

        public void SetPositionItems(IEnumerable<Position> positions)
        {
            foreach (var position in positions)
            {
                PositionItems.Add(new SelectListItem()
                {
                    Value = position.PositionId.ToString(),
                    Text = position.PositionName
                });
            }
        }
    }
}