﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseballLeague.Models
{
    public class Player
    {
        public int PlayerId { get; set; }
        public int TeamId { get; set; }
        public int PositionId { get; set; }
        public string PlayerName { get; set; }
        public byte Jersey { get; set; }
        public int BattingAvg { get; set; }
        public int YearsPlayed { get; set; }
    }
}