﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaseballLeague.Models;
using BaseballLeague.Repositories;

namespace BaseballLeague.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            BaseballRepository repo = new BaseballRepository();

            List<League> leagues = repo.GetAllLeagues();
            
            return View(leagues);
        }

        public ActionResult CreatePlayer()
        {
            return View(new Player());
        }

        public ActionResult CreateTeam()
        {
            return View(new Team());
        }
    }
}