﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using BaseballLeague.Config;
using BaseballLeague.Models;
using Dapper;

namespace BaseballLeague.Repositories
{
    public class BaseballRepository
    {
        public List<League> GetAllLeagues()
        {
            List<League> leaguesList;

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var leagues = cn.Query<League>("Select * from League").ToList();

                leaguesList = leagues;
            }
            return leaguesList;
        }
       
        private League PopulateRatingsFromDataReader(SqlDataReader dr)
        {
            League league = new League();

            league.LeagueId = (int) dr["LeagueId"];
            league.LeagueName = dr["LeagueName"].ToString();

            return league;
        }
    }
}