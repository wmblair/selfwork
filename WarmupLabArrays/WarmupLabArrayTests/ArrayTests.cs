﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WarmupLabArrays;

namespace WarmupLabArrayTests
{
    [TestFixture]
    public class ArrayTests
    {
        private Arrays array;

        [SetUp]
        public void RunBeforeTests()
        {
            array = new Arrays();
        }

        [TestCase(new int[] {1,6,2,4,6}, true)]
        [TestCase(new int[] {1,6,2,4,6,1,2,6,9}, false)]
        [TestCase(new int[] {6,1,2,4,6}, true)]
        [TestCase(new int[] {6,1,2,4,5,9,8}, true)]
        public void FirstLast6ReturnsExpected(int[] numbers, bool expected) {
            bool result = array.FirstLast6(numbers);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{1,2,3}, false)]
        [TestCase(new int[]{1,2,3,1}, true)]
        [TestCase(new int[]{1,2,1}, true)]
        public void SameFirstLastReturnsExpected(int[] numbers, bool expected) {
            bool result = array.SameFirstLast(numbers);
            Assert.AreEqual(result, expected);
        }

        [TestCase(5, new int[] {3,1,4,1,5})]
        [TestCase(9, new int[] {3,1,4,1,5,9,2,6,5})]
        public void MakePiReturnsExpected(int n, int[] expected) {
            int[] result = array.MakePi(n);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{1, 2, 3}, new int[]{7, 3},true)]
        [TestCase(new int[]{1, 2, 3}, new int[]{7, 3, 2},false)]
        [TestCase(new int[]{1, 2, 3}, new int[]{1, 3},true)]
        public void CommonEndReturnsExpected(int[] a, int[] b, bool expected) {
            bool result = array.CommonEnd(a,b);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[] {1, 2, 3}, 6)]
        [TestCase(new int[] {5, 11, 2}, 18)]
        [TestCase(new int[] {7, 0, 0}, 7)]
        public void SumReturnsExpected(int[] numbers, int expected) {
            int result = array.Sum(numbers);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{1, 2, 3},new int[]{2, 3, 1})]
        [TestCase(new int[]{5, 11, 9},new int[]{11, 9, 5})]
        [TestCase(new int[]{7, 0, 0},new int[]{0, 0, 7})]
        public void RotateLeftReturnsExpected(int[] numbers, int[] expected) {
            int[] result = array.RotateLeft(numbers);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{1, 2, 3},new int[]{3,2,1})]
        [TestCase(new int[]{5, 11, 9},new int[]{9, 11, 5})]
        [TestCase(new int[]{7, 0},new int[]{0, 7})]
        [TestCase(new int[]{7},new int[]{7})]
        [TestCase(new int[]{7, 4, 7, 3, 8, 2, 7, 8},new int[]{8,7,2,8,3,7,4,7})]
        public void ReverseReturnsExpected(int[] numbers, int[] expected) {
            int[] result = array.Reverse(numbers);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{1, 2, 3},new int[]{3, 3, 3})]
        [TestCase(new int[]{11, 5, 9},new int[]{11, 11, 11})]
        [TestCase(new int[]{7, 4, 7, 3, 8, 2, 7, 8},new int[]{8,8,8,8,8,8,8,8})]
        public void HigherWinsReturnsExpected(int[] numbers, int[] expected) {
            int[] result = array.HigherWins(numbers);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{1, 2, 3}, new int[]{4, 5, 6}, new int[]{2, 5})]
        [TestCase(new int[]{7, 7, 7}, new int[]{3, 8, 0}, new int[]{7, 8})]
        [TestCase(new int[]{5,2,9}, new int[]{1,4,5}, new int[]{2, 4})]
        public void GetMiddleReturnsExpected(int[] a, int[] b, int[] expected) {
            int[] result = array.GetMiddle(a,b);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{2,5}, true)]
        [TestCase(new int[]{4,3}, true)]
        [TestCase(new int[]{7,5}, false)]
        public void HasEvenReturnsExpected(int[] numbers, bool expected) {
            bool result = array.HasEven(numbers);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{4,5,6}, new int[]{0,0,0,0,0,6})]
        [TestCase(new int[]{1,2}, new int[]{0,0,0,2})]
        [TestCase(new int[]{3}, new int[]{0,3})]
        public void KeepLastReturnsExpected(int[] numbers, int[] expected) {
            int[] result = array.KeepLast(numbers);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{2,2,3}, true)]
        [TestCase(new int[]{3,4,5,3}, true)]
        [TestCase(new int[]{2,3,2,2}, false)]
        public void Double23ReturnsExpected(int[] numbers, bool expected) {
            bool result = array.Double23(numbers);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{1,2,3}, new int[]{1,2,0})]
        [TestCase(new int[]{2,3,5}, new int[]{2,0,5})]
        [TestCase(new int[]{1,2,1}, new int[]{1,2,1})]
        public void Fix23ReturnsExpected(int[] numbers, int[] expected) {
            int[] result = array.Fix23(numbers);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{1, 3, 4, 5},true)]
        [TestCase(new int[]{2, 1, 3, 4, 5},true)]
        [TestCase(new int[]{1, 1, 1},false)]
        public void Unlucky1ReturnsExpected(int[] numbers, bool expected)
        {
            bool result = array.Unlucky1(numbers);
            Assert.AreEqual(result, expected);
        }

        [TestCase(new int[]{4, 5}, new int[]{1, 2, 3}, new int[]{4, 5})]
        [TestCase(new int[]{4}, new int[]{1, 2, 3}, new int[]{4,1})]
        [TestCase(new int[]{}, new int[]{1,2}, new int[]{1,2})]
        public void Make2ReturnsExpected(int[] a, int[] b, int[] expected) {
            int[] result = array.Make2(a,b);
            Assert.AreEqual(result, expected);
        }
    }
}
