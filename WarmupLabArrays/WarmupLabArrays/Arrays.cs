﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarmupLabArrays
{
    public class Arrays
    {

        public bool FirstLast6(int[] numbers) {

            if (numbers.Length == 1)
            {
                if (numbers[0] == 6)
                    return true;
            }
            if (numbers[0] == 6 || numbers[numbers.Length - 1] == 6)
                return true;
            return false;
        }

        public bool SameFirstLast(int[] numbers) {
            if (numbers.Length >= 1)
            {
                if (numbers[0] == numbers[numbers.Length-1])
                    return true;
            }
            return false;
        }

         public int[] MakePi(int n) {

            double pi = Math.PI;
            string str = pi.ToString();
            str = str.Remove(1, 1);

            int[] piArray = new int[n];

            for (int i = 0; i < n; i++)
            {
               piArray[i] = Int32.Parse(str[i].ToString());
               Console.Write(piArray[i]);
            }
            return piArray;
        }

        public bool CommonEnd(int[] a, int[] b)
        {
            if (a.Length > 0 && b.Length > 0)
            {
                if (a[a.Length-1] == b[b.Length-1] || a[0] == b[0])
                    return true;
            }
            return false;
        }

        public int Sum(int[] numbers)
        {
            int sum = 0;
            foreach (int num in numbers)
                sum += num;
            return sum;
        }

        public int[] RotateLeft(int[] numbers)
        {
            int temp = numbers[0];
            for (int i = 0; i < numbers.Length-1; i++)
                numbers[i] = numbers[i + 1];
            numbers[numbers.Length - 1] = temp;
            return numbers;
        }

        public int[] Reverse(int[] numbers)
        {
            for (int i = 0; i < numbers.Length/2; i++)
            {
                int temp = numbers[i];
                numbers[i] = numbers[numbers.Length-1-i];
                numbers[numbers.Length-1-i] = temp;
            }
            return numbers;
        }

        public int[] HigherWins(int[] numbers)
        {
            int higher = numbers[0];
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > higher)
                higher = numbers[i];            
            }
            for (int i = 0; i < numbers.Length; i++)
                numbers[i] = higher;

            return numbers;
        }

        public int[] GetMiddle(int[] a, int[] b)
        {
            int[] middleArray = new int[] {a[a.Length/2], b[b.Length/2]};
            return middleArray;
        }

        public bool HasEven(int[] numbers)
        {
            foreach (int i in numbers)
            {
                if (i%2 == 0)
                    return true;
            }
            return false;
        }

        public int[] KeepLast(int[] numbers)
        {
            int[] doubleNumbers = new int[numbers.Length*2];
            doubleNumbers[doubleNumbers.Length - 1] = numbers[numbers.Length - 1];
            return doubleNumbers;
        }

        public bool Double23(int[] numbers)
        {
            int twoCount = 0, threeCount = 0;
            for (int i=0; i < numbers.Length; i++)
            {
                if (numbers[i] == 2)
                    twoCount++;
                if (numbers[i] == 3)
                    threeCount++;
            }
                if (twoCount == 2 || threeCount == 2)
                    return true;
            return false;
        }

        public int[] Fix23(int[] numbers)
        {
            for (int i = 0; i < numbers.Length-1; i++)
            {
                if (numbers[i] == 2 && numbers[i + 1] == 3)
                    numbers[i + 1] = 0;
            }
            return numbers;
        }

        public bool Unlucky1(int[] numbers) {

            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (i < 2 || i > numbers.Length - 3)
                {
                    if (numbers[i] == 1 && numbers[i + 1] == 3)
                        return true;
                }
            }
            return false;
        }

        public int[] Make2(int[] a, int[] b) {

            int[] twoArray = new int[2];

            if (a.Length == 0)
                twoArray = b;
            else if (a.Length == 1)
            {
                twoArray[0] = a[0];
                twoArray[1] = b[0];
            }
            else
                twoArray = a;
            return twoArray;
        }

    }
}
