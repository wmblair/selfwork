﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace ArrayReverseOrder
{
    class Program
    {
        static void Main(string[] args)
        {
            string s1 = "hello world";
            s1 = Reverse(s1);
            Console.WriteLine(s1);
            Console.ReadLine();
        }

        static string Reverse(string str)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(str);

            for (int i = 0; i < str.Length/2; i++)
            {
                char temp = str[i];
                sb[i] = sb[str.Length - 1 - i];
                sb[str.Length - 1 - i] = temp;
            }
            return sb.ToString();
        }
    }
}
