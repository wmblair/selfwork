﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFDemo.Models;

namespace EFDemo.Data
{
    public class EfDemoContext : DbContext
    {
        public DbSet<DVD> Dvds { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<MPAARating> MPAARatings { get; set; }

        public EfDemoContext():base("EfDemoContext")
        {
            
        }
    }
}
