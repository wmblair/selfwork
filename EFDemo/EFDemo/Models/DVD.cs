using System;
using System.Collections.Generic;

namespace EFDemo.Models
{
    public class DVD
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime ReleaseDate { get; set; }
        public virtual MPAARating MPAARating { get; set; }
        public virtual List<Director> Directors { get; set; }

    }
}