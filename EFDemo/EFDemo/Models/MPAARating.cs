using System.Collections.Generic;

namespace EFDemo.Models
{
    public class MPAARating
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<DVD> Dvds { get; set; }
    
    }
}