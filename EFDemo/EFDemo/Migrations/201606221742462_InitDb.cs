namespace EFDemo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Directors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DVDs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ReleaseDate = c.DateTime(nullable: false),
                        MPAARating_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPAARatings", t => t.MPAARating_Id)
                .Index(t => t.MPAARating_Id);
            
            CreateTable(
                "dbo.MPAARatings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DVDDirectors",
                c => new
                    {
                        DVD_Id = c.Int(nullable: false),
                        Director_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DVD_Id, t.Director_Id })
                .ForeignKey("dbo.DVDs", t => t.DVD_Id, cascadeDelete: true)
                .ForeignKey("dbo.Directors", t => t.Director_Id, cascadeDelete: true)
                .Index(t => t.DVD_Id)
                .Index(t => t.Director_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DVDs", "MPAARating_Id", "dbo.MPAARatings");
            DropForeignKey("dbo.DVDDirectors", "Director_Id", "dbo.Directors");
            DropForeignKey("dbo.DVDDirectors", "DVD_Id", "dbo.DVDs");
            DropIndex("dbo.DVDDirectors", new[] { "Director_Id" });
            DropIndex("dbo.DVDDirectors", new[] { "DVD_Id" });
            DropIndex("dbo.DVDs", new[] { "MPAARating_Id" });
            DropTable("dbo.DVDDirectors");
            DropTable("dbo.MPAARatings");
            DropTable("dbo.DVDs");
            DropTable("dbo.Directors");
        }
    }
}
