﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Antlr.Runtime.Misc;

namespace SGCorpJobSite.Models
{
    public static class ApplicantRepository
    {
        private static List<Applicant> _applicantList = new List<Applicant>();

        public static List<Applicant> GetApplicants()
        {
            return _applicantList;
        }

        public static Applicant GetApplicantById(int id)
        {
            return _applicantList.First(j => j.ApplicantId == id);
        }

        public static void AddApplicant(Applicant applicant)
        {
            if (_applicantList.Count == 0)
                applicant.ApplicantId = 1;
            else
                applicant.ApplicantId = _applicantList.Select(j => j.ApplicantId).Max() + 1;

            _applicantList.Add(applicant);
        }

        /*
        public static void ModifyJob(Job job)
        {
            if(_applicantList.Count > 0)
            _applicantList.RemoveAt(job.JobId - 1);

            _applicantList.Insert(job.JobId - 1, job);
        }
        */

        public static void DeleteJob(int id)
        {
            _applicantList.RemoveAt(id-1);
        }

        public static List<Job> GetJobsApplied(int id)
        {
            Applicant applicant = GetApplicantById(id);
            return applicant.JobsApplied;
        }

    }
}