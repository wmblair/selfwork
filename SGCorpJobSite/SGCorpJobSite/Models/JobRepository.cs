﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Antlr.Runtime.Misc;

namespace SGCorpJobSite.Models
{
    public static class JobRepository
    {
        private static List<Job> _jobsList;

        static JobRepository()
        {
            _jobsList = new List<Job>();

            Job j1 = new Job
            {
                JobId = 1,
                Title = "Junior C# Developer",
                Description = "Requires good object oriented skills and programming experience in C#",
                DateAdded = DateTime.Now,
                ApplicantList = new List<JobApplicant>()
            };

            Job j2 = new Job
            {
                JobId = 2,
                Title = "Senior C# Developer",
                Description = "Requires at least 10 years of C# experience",
                DateAdded = DateTime.Now,
                ApplicantList = new List<JobApplicant>()
            };

            Job j3 = new Job
            {
                JobId = 3,
                Title = "Junior Java Developer",
                Description = "Requires good object oriented skills and programming experience in Java",
                DateAdded = DateTime.Now,
                ApplicantList = new List<JobApplicant>()
            };

            _jobsList.Add(j1);
            _jobsList.Add(j2);
            _jobsList.Add(j3);

            Resume r1 = new Resume
            {
                Objective = "John Smith is looking for a coding job",
                Education = "Bachelor's Degree in CS at MIT",
                JobExperience = "2 years of experience as a Junior Developer at Google, inc."
            };

            Applicant a1 = new Applicant
            {
                ApplicantId = 1,
                FirstName = "John",
                LastName = "Smith",
                Email = "jsmith@gmail.com",
                Phone = "216-456-7890",
                City = "Cleveland",
                State = "Ohio",
                Zip = 44123,
            //    Resume = r1,
            };
            
            //John Smith applies for Junior C# developer
            _jobsList[0].ApplicantList.Add(new JobApplicant
            {
                Applicant = a1,
                Accepted = false,
                Interviewing = false,
                Rejected = false
            });
            
        }

        public static List<Job> GetJobs()
        {
            return _jobsList;
        }

        public static Job GetJobById(int id)
        {
            return _jobsList.First(j => j.JobId == id);
        }

        public static void AddJob(Job job)
        {
            if (_jobsList.Count == 0)
                job.JobId = 1;
            else
                job.JobId = _jobsList.Select(j => j.JobId).Max() + 1;

            _jobsList.Add(job);
        }

        public static void ModifyJob(Job job)
        {
            if(_jobsList.Count > 0)
            _jobsList.RemoveAt(job.JobId - 1);

            _jobsList.Insert(job.JobId - 1, job);
        }

        public static void DeleteJob(int id)
        {
            _jobsList.RemoveAt(id-1);
        }

        public static List<JobApplicant> GetApplicantList(int id)
        {
            Job job = GetJobById(id);
            return job.ApplicantList;
        }
    }
}