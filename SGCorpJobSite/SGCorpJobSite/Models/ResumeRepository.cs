﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGCorpJobSite.Models
{
    public static class ResumeRepository
    {
        private static List<Resume> _resumeList = new List<Resume>();

        public static List<Resume> GetResume()
        {
            return _resumeList;
        }

        public static Resume GetResumeFromApplicantId(int id)
        {
            return _resumeList.First(r => r.ApplicantId == id);
        }

        public static void AddResume(Resume resume)
        {
            if (_resumeList.Count == 0)
                resume.ResumeId = 1;
            else
                resume.ResumeId = _resumeList.Select(j => j.ResumeId).Max() + 1;

            _resumeList.Add(resume);
        }

        public static void ModifyResume(Resume resume)
        {
            if(_resumeList.Count > 0)
            _resumeList.RemoveAt(resume.ResumeId - 1);

            _resumeList.Insert(resume.ResumeId - 1, resume);
        }

        public static void DeleteResume(int id)
        {
            _resumeList.RemoveAt(id-1);
        }

        public static List<Resume> GetResumeList()
        { 
           return _resumeList;
        }

    }
}