﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGCorpJobSite.Models
{
    public class JobApplicant
    {
        public int JobId { get; set; }
        public Applicant Applicant { get; set; }
        public bool Accepted { get; set; }
        public bool Interviewing { get; set; }
        public bool Rejected { get; set; }
    }
}