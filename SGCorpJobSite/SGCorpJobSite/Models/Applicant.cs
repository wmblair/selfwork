﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SGCorpJobSite.Models
{
    public class Applicant
    {
        [Display(Name = "Applicant ID")]
        public int ApplicantId { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Resume Resume { get; set; }
        public List<Job> JobsApplied { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? Zip { get; set; }
    }
}