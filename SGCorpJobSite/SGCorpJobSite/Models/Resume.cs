﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGCorpJobSite.Models
{
    public class Resume
    {
        public int ResumeId { get; set; }
        public int ApplicantId { get; set; }
        public string Objective { get; set; }
        public string JobExperience { get; set; }
        public string Education { get; set; }
    }
}