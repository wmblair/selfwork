﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGCorpJobSite.Models.ViewModels
{
    public class PolicyVM
    {
        public List<Category> Categories { get; set; }
        public List<SelectListItem> CategoryItems { get; set; }
        public List<int> SelectedCategoryIds { get; set; }
        public Category Category { get; set; }

        public PolicyVM()
        {
            CategoryItems = new List<SelectListItem>();
            SelectedCategoryIds = new List<int>();
        }

        public void SetCategoryItems(IEnumerable<Category> categories)
        {
            foreach (var category in categories)
            {
                CategoryItems.Add(new SelectListItem()
                {
                    Value = category.CategoryId.ToString(),
                    Text = category.CategoryName
                });
            }
        }
    }
}