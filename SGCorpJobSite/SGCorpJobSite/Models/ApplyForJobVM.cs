﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGCorpJobSite.Models
{
    public class ApplyForJobVM
    {
        public Job Job { get; set; }
        public Applicant NewApplicant { get; set; }

        public ApplyForJobVM()
        {
            NewApplicant = new Applicant();
        }
    }
}