﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGCorpJobSite.Models
{
    public class Job
    {
        public int JobId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public List<JobApplicant> ApplicantList { get; set; }
    }
}