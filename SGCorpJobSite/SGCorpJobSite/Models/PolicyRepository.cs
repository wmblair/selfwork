﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGCorpJobSite.Models
{
    public static class PolicyRepository
    {
        private static List<Policy> _policyList = new List<Policy>();
        private static List<Category> _categoryList = new List<Category>();

        static PolicyRepository()
        {

        /*
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public List<Policy> Policies { get; set; }
        */

            Category c1 = new Category
            {
                CategoryId = 1,
                CategoryName = "Employees",
                Policies = new List<Policy>()
            };

            Category c2 = new Category
            {
                CategoryId = 2,
                CategoryName = "HR",
                Policies = new List<Policy>()
            };

            Category c3 = new Category
            {
                CategoryId = 3,
                CategoryName = "Company",
                Policies = new List<Policy>()
            };

            _categoryList.Add(c1);
            _categoryList.Add(c2);
            _categoryList.Add(c3);

            Policy p1 = new Policy
            {                
                CategoryId = 1,
                Title = "Dress Code",
                Content = "Employees should dress nicely"
            };  

            Policy p2 = new Policy
            {                
                CategoryId = 3,
                Title = "Privacy Policy",
                Content = "This policy is private"
            };  

            Policy p3 = new Policy
            {                
                CategoryId = 2,
                Title = "Hiring",
                Content = "HR should hire people"
            };  
            
            _policyList.Add(p1);
            _policyList.Add(p2);
            _policyList.Add(p3);

            //"Dress Code" "Privacy Policy" "Hiring"

            /*
        public int PolicyId { get; set; }
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }             
             */
        }

        public static List<Policy> GetPolicies()
        {
            return _policyList;
        }

        public static List<Category> GetCategories()
        {
            return _categoryList;
        }

        public static Category GetCategoryById(int id)
        {
            return _categoryList.First(c => c.CategoryId == id);
        }

        public static Policy GetPolicyById(int id)
        {
            return _policyList.First(p => p.PolicyId == id);
        }

        public static List<Policy> GetPoliciesById(int id)
        {
            return _policyList.Where(p => p.PolicyId == id).ToList();
        }

        public static Category GetCategoryByPolicyId(int policyId)
        {
            Policy policy = _policyList.First(p => p.PolicyId == policyId);
            Category category = _categoryList.First(c => c.CategoryId == policy.CategoryId);
            return category;
        }

        public static List<Policy> GetPoliciesByCategoryId(int categoryId)
        {
            return _policyList.Where(p => p.CategoryId == categoryId).ToList();
        }

        public static void AddPolicy(Policy policy)
        {
            if (_policyList.Count == 0)
                policy.PolicyId = 1;
            else
                policy.PolicyId = _policyList.Select(p => p.PolicyId).Max() + 1;

            _policyList.Add(policy);
        }

        /*
        public static void ModifyJob(Job job)
        {
            if(_jobsList.Count > 0)
            _jobsList.RemoveAt(job.JobId - 1);

            _jobsList.Insert(job.JobId - 1, job);
        }
        */

        public static void DeletePolicy(int id)
        {
            _policyList.RemoveAt(id-1);
        }

        /*
        public static List<JobApplicant> GetApplicantList(int id)
        {
            Job job = GetJobById(id);
            return job.ApplicantList;
        }
        */
    }
}