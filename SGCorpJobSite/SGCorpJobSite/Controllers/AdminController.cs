﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using SGCorpJobSite.Models;
using SGCorpJobSite.Models.ViewModels;

namespace SGCorpJobSite.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        /*
        public ActionResult Index()
        {
            return View();
        }
        */

        public ActionResult JobsListingAdmin()
        {
            var model = JobRepository.GetJobs();
            return View(model);
        }

        public ActionResult ApplicantsListing(int id)
        {
            var model = JobRepository.GetJobById(id); 
            //the reason you want to search by ID is to find a job object that was already instantiated
            return View(model);
        }

        public ActionResult AddNewJob()
        {

            return View(new Job());
        }

        [HttpPost]
        public ActionResult AddNewJob(Job job)
        {
            job.ApplicantList = new List<JobApplicant>();
            JobRepository.AddJob(job);

            return RedirectToAction("JobsListingAdmin");
        }

        public ActionResult ModifyJob(int id)
        {
            Job job = JobRepository.GetJobById(id);

            return View(job);
        }

        [HttpPost]
        public ActionResult ModifyJob(Job job)
        {
            job.ApplicantList = JobRepository.GetApplicantList(job.JobId); 
            //need to retrieve the applicantlist for the job again

            JobRepository.ModifyJob(job);

            return RedirectToAction("JobsListingAdmin");
        }

        public ActionResult DeleteJob(int id)
        {
            Job job = JobRepository.GetJobById(id);
            JobRepository.DeleteJob(job.JobId);

            return RedirectToAction("JobsListingAdmin");
        }

        public ActionResult ResumeListing(int id)
        {
            Resume resume = ResumeRepository.GetResumeFromApplicantId(id);
            return View(resume);
        }

        /*
        public ActionResult ResumeListingBack()
        {
            return RedirectToAction("ApplicantsListing");
        }
        */

        public ActionResult AddNewPolicy()
        {
            AddPolicyVM addPolicy = new AddPolicyVM
            {
                Categories = PolicyRepository.GetCategories(),
            };
            addPolicy.SetCategoryItems(addPolicy.Categories);
            return View(addPolicy);
        }

        [HttpPost]
        public ActionResult AddNewPolicy(AddPolicyVM addPolicyVM)
        {
            PolicyRepository.AddPolicy(addPolicyVM.NewPolicy);
            return RedirectToAction("ManagePolicies");
        }

        public ActionResult ManagePolicies()
        {
            PolicyVM pvm = new PolicyVM {Category = new Category()};
            pvm.SetCategoryItems(PolicyRepository.GetCategories());
            return View(pvm);
        }

        [HttpPost]
        public ActionResult ManagePolicies(PolicyVM policyVM, string viewPolicies, string addNewPolicy)
        {
            if (!string.IsNullOrEmpty(viewPolicies))
            {
                return RedirectToAction("CategorySelection", policyVM.Category);
            }
            return RedirectToAction("AddNewPolicy");
        }

        public ActionResult ViewPolicies()
        {
            PolicyVM pvm = new PolicyVM {Category = new Category()};
            pvm.SetCategoryItems(PolicyRepository.GetCategories());
            return View(pvm);
        }

        [HttpPost]
        public ActionResult CategorySelection(Category category)
        {
            category.Policies = PolicyRepository.GetPoliciesByCategoryId(category.CategoryId);
            return View(category);
        }



    }
}

//model binding
//many to many relationship between two tables (three tables)