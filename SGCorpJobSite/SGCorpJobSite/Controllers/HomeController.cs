﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGCorpJobSite.Models;

namespace SGCorpJobSite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult JobsListing()
        {
            var model = JobRepository.GetJobs();
            return View(model);
        }

        public ActionResult ApplyForJob(int id)
        {
            var jobApplicant = new JobApplicant
            {         
                JobId = id,          
                Accepted = false,               
                Interviewing = false,
                Rejected = false
            };

            // Applicant = new Applicant(), //using this you can output information from this object in the view
            //but in this case, don't need to retrieve anthing from it

            return View(jobApplicant);
        }

        [HttpPost]
        public ActionResult ApplyForJob(JobApplicant jobAppl)
        {
            Job job = JobRepository.GetJobById(jobAppl.JobId); //model knows what data it has when being passed into view, but not when coming back out for post

            ApplicantRepository.AddApplicant(jobAppl.Applicant);

            job.ApplicantList.Add(jobAppl);

            jobAppl.Applicant.Resume.ApplicantId = jobAppl.Applicant.ApplicantId;
            ResumeRepository.AddResume(jobAppl.Applicant.Resume);

            //in a view, if an object within the model isn't used in a labelfor or placeholder or similar, 
            //it will be a NULL value when the object parameter receives the view model information
            //job ID had to be put in a hiddenfor so that jobAppl knows what ID Job has!

            JobRepository.ModifyJob(job); //might not need this

            return RedirectToAction("JobsListing");
        }

        /*
        public ActionResult AddResume(JobApplicant jobAppl)
        {
            return View(jobAppl); 
            //create the new applicant if they don't have any resumes or jobs applied to, otherwise just retrieve the applicant from the repo
        }
           
        [HttpPost]
        public ActionResult AddResumePost(JobApplicant jobAppl)
        {
            //jobAppl.Applicant.Resume.ApplicantId = ApplicantRespository
            ResumeRepository.AddResume(jobAppl.Applicant.Resume);
            return RedirectToAction("ApplyForJob", jobAppl);
        }
        
        [HttpPost]
        public ActionResult AddResumePost(JobApplicant jobApplicant)
        {
            ResumeRepository.AddResume(jobApplicant.Applicant.Resume);
            return RedirectToAction("ApplyForJob", jobApplicant);
        }
        */
    }
}