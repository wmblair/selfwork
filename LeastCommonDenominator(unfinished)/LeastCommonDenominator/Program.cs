﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeastCommonDenominator
{
    class Program
    {

        static decimal GetEnumerator(string input)
        {
            int digit;
            int.TryParse(input[0].ToString(), out digit);
            return digit;
        }

        static decimal GetDenominator(string input)
        {
            int digit;
            int.TryParse(input[1].ToString(), out digit);
            return digit;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter a fraction: ");
            string input = Console.ReadLine(); //in the form of a double digit number for now

            decimal frac = GetEnumerator(input) / GetDenominator(input);

            Console.WriteLine("Enter another fraction: ");
            string input2 = Console.ReadLine();

            decimal frac2 = GetEnumerator(input2) / GetDenominator(input2);

            int length = Math.Max(Int32.Parse(GetDenominator(input).ToString()), 
                Int32.Parse(GetDenominator(input2).ToString()));

            for (int i = 2; i < length; i++)
            {
                if (length % i == 0) //see if denominator is a multiple of some number
                {                
                    if ((input[0]/i)/(input[1]/i) == (input2[0]/i)/(input2[1]/i))
                    {
                        Console.WriteLine("lcd is " + i);
                        break;
                    }
                }
            }
            Console.ReadLine();
        }
    }
}
