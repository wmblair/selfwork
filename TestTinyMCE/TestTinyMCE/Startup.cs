﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestTinyMCE.Startup))]
namespace TestTinyMCE
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
