﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace WarmupLabLogic
{
    public class Logic
    {
        public bool GreatParty(int cigars, bool isWeekend)
        {
            if (isWeekend)
            {
                if (cigars >= 40)               
                    return true;

                return false;
            }

            if (cigars >= 40 && cigars <= 60)
                return true;

            return false;
        }

        public int CanHazTable(int yourStyle, int dateStyle)
        {
            if (yourStyle >= 8 && yourStyle <= 10 || dateStyle >= 8 && dateStyle <= 10)
                return 2; //(yes)
            if (yourStyle >= 0 && yourStyle <= 2 || dateStyle >= 0 && dateStyle <= 2)
                return 0; //(no)
            return 1; //(maybe)
        }

        public bool PlayOutside(int temp, bool isSummer)
        {
            if (isSummer)
            {
                if (temp >= 60 && temp <= 100)
                    return true;
                return false;
            }
            if (temp >= 60 && temp <= 90)
                return true;
            return false;
        }

        public int CaughtSpeeding(int speed, bool isBirthday)
        {
            if (isBirthday)
            {
                if (speed <= 65)
                    return 0;
                if (speed > 65 && speed <= 85)
                    return 1;
                return 2;
            }
            if (speed <= 60)
                return 0;
            if (speed > 60 && speed <= 80)
                return 1;
            return 2;
        }

            //65 or less, 0, 66-85, 1, 86 or more, 2 (on birthday)
            //60 or less, 0, 61-80, 1, 81 or more, 2

        public int SkipSum(int a, int b)
        {
            if (a+b >= 10 && a+b <= 19)
                return 20;
            return a + b;
        }
        public string AlarmClock(int day, bool vacation)
        {
            if (day >= 1 && day <= 5)
            {
                if (vacation)
                    return "10:00";
                return "7:00";
            }
            if (day == 6 || day == 0)
            {
                if (vacation)
                    return "off";
                return "10:00";
            }
            return "";
        }

        public bool LoveSix(int a, int b)
        {
            if (a == 6 || b == 6)
                return true;
            if (a + b == 6 || (a - b == 6 || b - a == 6))
                return true;
            return false;
        }

        public bool InRange(int n, bool outsideMode)
        {
            if (!outsideMode)
            {
                if (n > 0 && n <= 10)
                    return true;
                return false;
            }
            if (n < 0 || n > 10)
                return true;
            return false;
        }

        public bool SpecialEleven(int n)
        {
            if (n >= 0) {
                if (n % 11 == 0 || n % 11 == 1)
                    return true;
            }
            return false;

        }

        public bool Mod20(int n)
        {
            if (n >= 0)
            {
                if (n % 20 == 1 || n % 20 == 2)
                    return true;
            }
            return false;
        }

        public bool Mod35(int n)
        {
            if (n >= 0)
            {
                if(n % 3 == 0 || n % 5 == 0)
                    return true;
            }
            return false;
        }

        public bool AnswerCell(bool isMorning, bool isMom, bool isAsleep) {
            if(isAsleep)
            {
                if(isMorning)  //if it is morning, but isn't your mom calling and you aren't asleep
                {
                    if(isMom)
                        return true;
                    return false;
                }
                return false;
            }
            if (isMorning)
                return false;
            return true;
        }

        public bool TwoIsOne(int a, int b, int c) {
            if(c == a+b || b == a+c || a == b+c)           
                return true;
            return false;
        }

        public bool AreInOrder(int a, int b, int c, bool bOk) {
            if(!bOk)
            {
                if (a < b && b < c)
                    return true;
            return false;
            }
                if (b < c)
                    return true;
            return false;
        }

        public bool InOrderEqual(int a, int b, int c, bool equalOk) {
            if(!equalOk)
            {
                if (a < b && b < c)
                    return true;
            return false;
            }
                if (a <= b && b <= c)
                    return true;
            return false;  
        }

        public bool LastDigit(int a, int b, int c)
        {
            if(a >= 0 && b >= 0 && c >= 0) {
                if (a % 10 == b % 10 || b % 10 == c % 10 || c % 10 == a % 10)
                    return true;
            }
            return false;
        }

        public int RollDice(int die1, int die2, bool noDoubles) {
            if(!noDoubles)
            {
                if((die1 >= 1 && die1 <= 6) && (die2 >= 1 && die2 <= 6))
                    return die1 + die2;
            }
            if(die1 == 6 && die2 == 6)
                return 1;
            if(die1 == die2)
                return die1 + die2 + 1;
            return die1 + die2;
        }
    }
}
