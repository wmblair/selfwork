﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using WarmupLabLogic;

namespace WarmupLabLogicTests
{
    [TestFixture]
    public class LogicTests
    {
        private Logic _logic;

        [SetUp]
        public void RunBeforeTests()
        {
            _logic = new Logic();
        }

        [TestCase(30, false, false)]
        [TestCase(50, false, true)]
        [TestCase(70, true, true)]
        public void GreatPartyReturnsExpected(int cigars, bool isWeekend, bool expected) {
            bool result = _logic.GreatParty(cigars, isWeekend);
            Assert.AreEqual(result, expected);
        }

        [TestCase(5, 10, 2)]
        [TestCase(5, 2, 0)]
        [TestCase(5,5,1)]
        public void CanHazTableReturnsExpected(int a, int b, int expected) {
            int result = _logic.CanHazTable(a,b);
            Assert.AreEqual(result, expected);
        }

        [TestCase(70, false, true)]
        [TestCase(95, false, false)]
        [TestCase(95, true, true)]
        public void PlayOutsideReturnsExpected(int temp, bool isSummer, bool expected) {
            bool result = _logic.PlayOutside(temp, isSummer);
            Assert.AreEqual(result, expected);
        }

        [TestCase(60,false,0)]
        [TestCase(65,false,1)]
        [TestCase(60,true,0)]
        public void CaughtSpeedingReturnsExpected(int speed, bool isBirthday, int expected) {
            int result = _logic.CaughtSpeeding(speed, isBirthday);
            Assert.AreEqual(result, expected);
        }

        [TestCase(3,4,7)]
        [TestCase(9,4,20)]
        [TestCase(10,11,21)]
        public void SkipSumReturnsExpected(int a, int b, int expected) {
            int result = _logic.SkipSum(a,b);
            Assert.AreEqual(result, expected);
        }

        [TestCase(1,false,"7:00")]
        [TestCase(5,false,"7:00")]
        [TestCase(0,false,"10:00")]
        public void AlarmClockReturnsExpected(int day, bool vacation, string expected) {
            string result = _logic.AlarmClock(day,vacation);
            Assert.AreEqual(result, expected);
        }

        [TestCase(6,4,true)]
        [TestCase(4,5,false)]
        [TestCase(1,5,true)]
        public void LoveSixReturnsExpectedt(int a, int b, bool expected) {
            bool result = _logic.LoveSix(a,b);
            Assert.AreEqual(result, expected);
        }

        [TestCase(5,false,true)]
        [TestCase(11,false,false)]
        [TestCase(11,true,true)]
        public void InRangeReturnsExpected(int n, bool outsidemode, bool expected) {
            bool result = _logic.InRange(n,outsidemode);
            Assert.AreEqual(result, expected);
        }

        [TestCase(22,true)]
        [TestCase(23,true)]
        [TestCase(24,false)]
        public void SpecialElevenReturnsExpected(int n, bool expected) {
            bool result = _logic.SpecialEleven(n);
            Assert.AreEqual(result, expected);
        }

        [TestCase(20,false)]
        [TestCase(21,true)]
        [TestCase(22,true)]
        public void Mod20ReturnsExpected(int n, bool expected) {
            bool result = _logic.Mod20(n);
            Assert.AreEqual(result, expected);
        }

        [TestCase(3,true)]
        [TestCase(10,true)]
        [TestCase(15,true)]
        public void Mod35ReturnsExpected(int n, bool expected) {
            bool result = _logic.Mod35(n);
            Assert.AreEqual(result, expected);
        }

        [TestCase(false,false,false,true)]
        [TestCase(false,false,true,false)]
        [TestCase(true,false,false,false)]
        public void AnswerCellReturnsExpected(bool isMorning, bool isMom, bool isAsleep, bool expected)
        {
            bool result = _logic.AnswerCell(isMorning, isMom, isAsleep);
            Assert.AreEqual(result, expected);
        }

        [TestCase(1,2,3,true)]
        [TestCase(3,1,2,true)]
        [TestCase(3,2,2,false)]
        public void TwoIsOneReturnsExpected(int a, int b, int c, bool expected)
        {
            bool result = _logic.TwoIsOne(a,b,c);
            Assert.AreEqual(result, expected);
        }

        [TestCase(1,2,4,false,true)]
        [TestCase(1,2,1,false,false)]
        [TestCase(1,1,2,true,true)]
        public void AreInOrderReturnsExpected(int a, int b, int c, bool bOk, bool expected)
        {
            bool result = _logic.AreInOrder(a,b,c,bOk);
            Assert.AreEqual(result, expected);
        }

        [TestCase(2,5,11,false,true)]
        [TestCase(5,7,6,false,false)]
        [TestCase(5,5,7,true,true)]
        public void InOrderEqualReturnsExpected(int a, int b, int c, bool equalOk, bool expected)
        {
            bool result = _logic.InOrderEqual(a,b,c,equalOk);
            Assert.AreEqual(result, expected);
        }

        [TestCase(23,19,13,true)]
        [TestCase(23,19,12,false)]
        [TestCase(23,19,3,true)]
        public void LastDigitReturnsExpected(int a, int b, int c, bool expected)
        {
            bool result = _logic.LastDigit(a,b,c);
            Assert.AreEqual(result, expected);
        }

        [TestCase(2,3,true,5)]
        [TestCase(3,3,true,7)]
        [TestCase(3,3,false,6)]
        public void RollDiceReturnsExpected(int a, int b, bool noDoubles, int expected)
        {
            int result = _logic.RollDice(a,b,noDoubles);
            Assert.AreEqual(result, expected);
        }

    }
}
