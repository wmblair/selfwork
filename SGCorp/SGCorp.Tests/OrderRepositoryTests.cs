﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGCorp.Data;
using SGCorp.Models;
using SGCorp.UI;

namespace SGCorp.Tests
{
    [TestFixture]
    public class OrderRepositoryTests
    {
        private OrderRepository repo;
        private Order testOrder;

        [SetUp]
        public void RunBeforeTests()
        {
            repo = new OrderRepository();

            //State: 
            //PA: 6.75
            //OH: 6.25
            //IN: 6.00
            //MI: 5.75

            decimal taxRate = 6.25m;

            //Product: 
            //Carpet: 2.25m, 2.10m
            //Laminate: 1.75m, 2.10m
            //Tile: 3.50m, 4.15m
            //Wood: 5.15m, 4.75m  

            decimal costPerSqFoot = 3.50m;
            decimal laborPerSqFoot = 4.15m;

            decimal area = 33.5m;

            decimal materialsCost = Calculations.CalculateMaterialsCost(area, costPerSqFoot);
            decimal laborCost = Calculations.CalculateLaborCost(area, laborPerSqFoot);
            decimal taxAmount = Calculations.CalculateTaxAmount(materialsCost, laborCost, taxRate);
            decimal totalCost = Calculations.CalculateTotalCost(materialsCost, laborCost, taxAmount);

            testOrder = new Order
            {
                CustomerName = "Will",
                StateName = "Ohio",
                StateAbbrev = "OH",
                TaxRate = taxRate,
                ProductType = "Tile",
                CostPerSqFoot = costPerSqFoot,
                LaborPerSqFoot = laborPerSqFoot,
                Area = area,
                MaterialsCost = materialsCost,
                LaborCost = laborCost,
                TaxAmount = taxAmount,
                TotalCost = totalCost,
                IsMoved = false,
                IsActive = true,
                DateOfOrder = DateTime.Now,
                
                //DateTime.ParseExact(s, "MMddyyyy", CultureInfo.InvariantCulture)
            };
        }

        [TestCase("05232016")]
        public void CanLoadAllOrders(string date)
        {
            DateTime dateTime = DateTime.ParseExact(date, "MMddyyyy", CultureInfo.InvariantCulture);
            var orders = repo.DisplayOrders(dateTime);
            Assert.AreEqual(orders.Count, 7);
        }      
        
        [TestCase("Will", "05232016")]
        public void CreateOrderSucceeds(string name, string date)
        {
            DateTime dateTime = DateTime.ParseExact
                (testOrder.DateOfOrder.ToString("MMddyyyy"), "MMddyyyy", CultureInfo.InvariantCulture);

            var createdOrder = repo.CreateOrder(testOrder.DateOfOrder, testOrder);
            
            Assert.AreEqual(testOrder.CustomerName, createdOrder.CustomerName);
            Assert.AreEqual(dateTime, createdOrder.DateOfOrder);       
        }
         

        /*
        [TestCase(1, "Mary")]
        [TestCase(2, "Bob")]
        public void CanLoadSpecificAccount(int accountNumber, string expected)
        {
            var repo = new AccountRepository();
            var account = repo.LoadAccount(accountNumber);

            Assert.AreEqual(expected, account.FirstName);
        }

        [Test]
        public void UpdateAccountSucceeds()
        {
            var repo = new AccountRepository();
            var accountToUpdate = repo.LoadAccount(1);
            accountToUpdate.Balance = 500.00m;
            repo.UpdateAccount(accountToUpdate);

            var result = repo.LoadAccount(1);
            Assert.AreEqual(500.00m, result.Balance);
        }

        [Test]
        public void CreateAccountSucceeds()
        {
            var repo = new AccountRepository();
            Account newAccount = new Account()
            {
                FirstName = "Mike", 
                LastName = "Popowicz",
                Balance = 2000
            };
            
            var createdAccount = repo.CreateAccount(newAccount);
            
            Assert.AreEqual(newAccount.FirstName, createdAccount.FirstName);
            Assert.AreEqual(newAccount.LastName, createdAccount.LastName);
            Assert.AreEqual(newAccount.Balance, createdAccount.Balance);
        }

        [Test]
        public void CreateAccountSelectsNextID()
        {
            var repo = new AccountRepository();
            Account newAccount = new Account()
            {
                FirstName = "Mike",
                LastName = "Popowicz",
                Balance = 2000
            };

            int nextID = repo.GetAllAccounts().Select(a => a.AccountNumber).Max() + 1;

            var createdAccount = repo.CreateAccount(newAccount);

            Assert.AreEqual(nextID, createdAccount.AccountNumber);
            
        }

        [Test]
        public void DeleteAccountSucceeds()
        {
            var repo = new AccountRepository();
            Account newAccount = new Account()
            {
                FirstName = "Mike",
                LastName = "Popowicz",
                Balance = 2000
            };

            var createdAccount = repo.CreateAccount(newAccount);

            Assert.AreEqual(newAccount.FirstName, createdAccount.FirstName);
            Assert.AreEqual(newAccount.LastName, createdAccount.LastName);
            Assert.AreEqual(newAccount.Balance, createdAccount.Balance);

            repo.DeleteAccount(createdAccount.AccountNumber);

            Assert.IsNull(repo.LoadAccount(createdAccount.AccountNumber));

        }

        [TestCase(0)]
        [TestCase(133)]
        public void DeleteAccountDoesntExist(int accountNumber)
        {
            var repo = new AccountRepository();
            
            var result = repo.DeleteAccount(accountNumber);

            Assert.IsFalse(result);

        }

    */
    }
}
