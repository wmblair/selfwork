﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGCorp.BLL;
using SGCorp.Models;
using SGCorp.UI;

namespace SGCorp.Tests
{
    [TestFixture]
    public class OrderManagerTests
    {
        private OrderManager om;
        private Order testOrder;

        [SetUp]
        public void RunBeforeTests()
        {
            om = new OrderManager();

            //State: 
            //PA: 6.75
            //OH: 6.25
            //IN: 6.00
            //MI: 5.75

            decimal taxRate = 6.25m;

            //Product: 
            //Carpet: 2.25m, 2.10m
            //Laminate: 1.75m, 2.10m
            //Tile: 3.50m, 4.15m
            //Wood: 5.15m, 4.75m  

            decimal costPerSqFoot = 3.50m;
            decimal laborPerSqFoot = 4.15m;

            decimal area = 33.5m;

            decimal materialsCost = Calculations.CalculateMaterialsCost(area, costPerSqFoot);
            decimal laborCost = Calculations.CalculateLaborCost(area, laborPerSqFoot);
            decimal taxAmount = Calculations.CalculateTaxAmount(materialsCost, laborCost, taxRate);
            decimal totalCost = Calculations.CalculateTotalCost(materialsCost, laborCost, taxAmount);

            testOrder = new Order
            {
                CustomerName = "John Snow",
                StateName = "Ohio",
                StateAbbrev = "OH",
                TaxRate = taxRate,
                ProductType = "Tile",
                CostPerSqFoot = costPerSqFoot,
                LaborPerSqFoot = laborPerSqFoot,
                Area = area,
                MaterialsCost = materialsCost,
                LaborCost = laborCost,
                TaxAmount = taxAmount,
                TotalCost = totalCost,
                IsMoved = false,
                IsActive = true,
                DateOfOrder = DateTime.Now,
                
                //DateTime.ParseExact(s, "MMddyyyy", CultureInfo.InvariantCulture)
            };
        }

        [Test]
        public void CreateNewOrderReturnsSuccess()
        {
            Order t1 = om.AddOrder(testOrder);

            Assert.IsTrue(om.IsSuccess);
            Assert.AreEqual(testOrder.DateOfOrder.ToString("MMddyyyy"), t1.DateOfOrder.ToString("MMddyyyy")); //see if load order retrieves the right order you just added
            Assert.AreEqual(testOrder.OrderId, t1.OrderId);
        }                   //expected, actual

        [TestCase(05202016)]
        [TestCase(05212016)]
        public void EditOrderReturnsSuccess(string date)
        {
            DateTime dateTime = DateTime.ParseExact(date, "MMddyyyy", CultureInfo.InvariantCulture);
            Order t1 = om.EditOrder(dateTime, testOrder); //have to test input to validate putting empty strings
            Assert.IsTrue(om.IsSuccess);
            Assert.AreEqual(testOrder, t1);
        } //make a new order object of what you want to change to for the original order object, then compare

        [TestCase("05232016", 2, "John Snow")]
        public void GetOrderReturnsSuccess(string date1, int orderId, string expectedName) //order id, customer name, date, state, product
        {
            DateTime dateTime = DateTime.ParseExact(date1, "MMddyyyy", CultureInfo.InvariantCulture);

            Order t1 = om.GetOrder(dateTime, orderId);
            Assert.IsTrue(om.IsSuccess);
            Assert.AreEqual(t1.CustomerName, expectedName);
            Assert.AreEqual(t1.DateOfOrder, dateTime);
        }

        public void DeleteOrderReturnsSuccess()
        {
            
        }

    }
}
