﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.UI.Workflows;

namespace SGCorp.UI
{
    public static class WorkflowFactory
    {
        public static IWorkflow WorkflowSelection(string input)
        {
            switch (input)
            {
                case "1":
                    return new DisplayOrderWorkflow();
                case "2":
                    return new CreateOrderWorkflow();
                case "3":
                    return new EditOrderWorkflow();
                case "4":
                    return new RemoveOrderWorkflow();

                default:
                    return null;
            }           
        }
    }
}
