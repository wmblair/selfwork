﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.BLL;

namespace SGCorp.UI.Utilities
{
    public static class StringToDate
    {
        public static DateTime StringToDateTime()
        {
            DateTime dateTime;
            do
            {
                try
                {
                    string date = Console.ReadLine();
                    if (date == "")
                        continue;
                    dateTime = DateTime.ParseExact(date, "MMddyyyy", CultureInfo.InvariantCulture);
                    break;                   
                }
                catch (FormatException fe)
                {
                    Console.Write("Invalid date format.  Please try again (MMddyyyy) : ");
                    OrderManager om = new OrderManager();
                    om.WriteToFile(fe.Message);
                }
            } while (true);

            return dateTime;
        }
    }
}
