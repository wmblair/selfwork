﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCorp.UI.Utilities
{
    public static class DateToString
    {
        public static string ConvertDateToString(DateTime date)
        {
            return Convert.ToString(date);
        }
    }
}
