﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCorp.UI.Utilities
{
    public static class PromptUserIfSure
    {
        public static bool PromptToConfirm(string prompt)
        {
            Console.Write(prompt + ": ");
            do
            {               
                string yesOrNo = Console.ReadLine();

                if (string.IsNullOrEmpty(yesOrNo)) {
                    continue;
                }
                if (yesOrNo.ToUpper() == "Y")
                    return true;
                if (yesOrNo.ToUpper() == "N") {
                    Console.Write("Going back to the main menu.  Press any key to continue...");
                    Console.ReadLine();
                    return false;
                }
                Console.Write("Your input is not recognized.  Please try again: ");
                               
            } while (true);
        }

    }
}
