﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Models;

namespace SGCorp.UI.Utilities
{
    public static class CheckAvailableStates
    {
        public static State GetState(string state, ArrayList states)
        {
            foreach (State st in states)
            {
                if (st.StateAbbrev == state || st.StateName == state)
                    return st;                
            }
            return null;
        }
    }
}
