﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.BLL;

namespace SGCorp.UI.Utilities
{
    public static class InputValidator
    {
        public static string UseStringIfValid()
        {
            string input;
            OrderManager om;
            do
            {
                try
                {
                    input = Console.ReadLine();
                    if (input != "")
                        break;
                }
                catch (NullReferenceException nre)
                {
                    Console.Write(nre + " ");
                    om = new OrderManager();
                    om.WriteToFile(nre.Message);
                }
                catch (Exception ex)
                {
                    Console.Write("The input you have entered is invalid.  Try again: ");
                    om = new OrderManager();
                    om.WriteToFile(ex.Message);
                }
            }
            while (true);

            return input;
        }
    }
}
