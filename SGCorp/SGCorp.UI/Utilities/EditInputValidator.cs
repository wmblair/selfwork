﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.BLL;

namespace SGCorp.UI.Utilities
{
    public static class EditInputValidator
    {
        public static string CheckIfBlank()
        {
            OrderManager om;
            string input;
            do
            {
                try
                {
                    input = Console.ReadLine();
                    if (input == "")
                        return " ";
                    break;
                }
                catch (NullReferenceException nre)
                {
                    Console.Write(nre + " ");
                    om = new OrderManager();
                    om.WriteToFile(nre.ToString());
                }
                catch (Exception ex)
                {
                    Console.Write("The input you have entered is invalid.  Try again: ");
                    om = new OrderManager();
                    om.WriteToFile(ex.Message);
                }
            }
            while (true);
            return input;
        }

        public static DateTime CheckIfBlank(DateTime date)
        {
            OrderManager om;
            do
            {
                try
                {
                    string input = Console.ReadLine();
                    if (input == "")
                        break;
                    date = DateTime.ParseExact(input, "MMddyyyy", CultureInfo.InvariantCulture);
                        break;
                }
                catch (Exception ex)
                {
                    Console.Write("Invalid date format.  Please try again (MMddyyyy) : ");
                    om = new OrderManager();
                    om.WriteToFile(ex.Message);
                }

            }
            while (true);
            return date;
        }
    }
}
