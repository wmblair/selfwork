﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Models;

namespace SGCorp.UI.Utilities
{
    public static class DisplayDetails
    {
        public static void DisplayAllOrderDetails(List<Order> list)
        {
            foreach (var l in list) {
                if (l.IsMoved)
                Console.WriteLine("--------------" + l.OrderId + "--------(moved)");
                else if (!l.IsActive)
                Console.WriteLine("--------------" + l.OrderId + "------(deleted)");
                else
                Console.WriteLine("--------------" + l.OrderId + "---------------");
                Console.WriteLine(l.CustomerName + "\n"
                                  + l.MaterialsCost + "\n"
                                  + l.LaborCost + "\n"
                                  + l.TotalCost + "\n"
                                  + l.TaxAmount + "\n"
                                  + l.Area + "\n"
                                  + l.ProductType + "\n"
                                  + l.CostPerSqFoot + "\n"
                                  + l.LaborPerSqFoot + "\n"
                                  + l.StateName + "\n"
                                  + l.StateAbbrev + "\n"
                                  + l.TaxRate + "\n"
                                  + l.DateOfOrder.ToString("MM/dd/yyyy") + "\n");
                 }
        }

    }
}
