﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Models;

namespace SGCorp.UI.Utilities
{
    public static class CheckAvailableProducts
    {
        public static Product GetProduct(string productType, ArrayList products)
        {
            foreach (Product p in products)
            {
                if (p.ProductType == productType)
                    return p;                
            }
            return null;
        }
    }
}
