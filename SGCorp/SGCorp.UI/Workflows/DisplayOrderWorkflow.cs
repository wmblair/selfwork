﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.BLL;
using SGCorp.Models;
using SGCorp.UI.Utilities;

namespace SGCorp.UI.Workflows
{
    public class DisplayOrderWorkflow : IWorkflow
    {
        private OrderManager orderManager;

        public DisplayOrderWorkflow()
        {
            orderManager = new OrderManager();
        }

        public void Execute()
        {
            Console.Write("Enter the date for the orders: ");
            int count = 0;
           
                DateTime date = StringToDate.StringToDateTime();

                List<Order> orderList = orderManager.DisplayAllOrders(date);

                if (!orderManager.IsSuccess)
                    Console.Write("Unable to retrieve date information.");

                else if (orderList.Count == 0)
                    Console.WriteLine("There are currently no orders using this date.");              
                else
                {
                    DisplayDetails.DisplayAllOrderDetails(orderList);
                    foreach (var order in orderList) {
                        if (!order.IsMoved && order.IsActive)
                            count++;
                    }
                    Console.WriteLine("There are currently " + count + " orders using this date.");
                }

            Console.ReadLine();
        }
    }
}
