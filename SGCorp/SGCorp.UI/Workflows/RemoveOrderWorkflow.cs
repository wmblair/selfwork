﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.BLL;
using SGCorp.Models;
using SGCorp.UI.Utilities;

namespace SGCorp.UI.Workflows
{
    public class RemoveOrderWorkflow : IWorkflow
    {
        private OrderManager orderManager;

        public RemoveOrderWorkflow()
        {
            orderManager = new OrderManager();
        }

        public void Execute()
        {
            int orderNumber = 20;

            Console.Write("Enter date file that the order you wish to remove is in (MMddyyyy): ");
            DateTime date = StringToDate.StringToDateTime();

            List<Order> orderList = orderManager.DisplayAllOrders(date);
            DisplayDetails.DisplayAllOrderDetails(orderList);

            int intOrderId;
            do
            {
                string orderId = InputValidator.UseStringIfValid();
                if (!int.TryParse(orderId, out intOrderId))
                    continue;

                Order orderToModify = orderManager.GetOrder(date, intOrderId);
                if (orderToModify == null)
                    Console.Write("There is no order ID in the file you specified! Try entering again: ");
                else if(!orderToModify.IsActive || orderToModify.IsMoved) {
                    Console.Write("This order was moved/deleted.  Press any key to continue...");
                    Console.ReadLine();
                    MainMenu.Execute();
                }
                else
                    break;
            } while (true);

            if(PromptUserIfSure.PromptToConfirm("Are you sure you want to Remove this Order? (Y)es or (N)o: ")) {
                orderManager.DeleteOrder(date, intOrderId);

            if (orderManager.IsSuccess == false)
                Console.WriteLine("order cannot be removed");
            else
                Console.WriteLine("order successfully removed");

                Console.ReadLine();
            }
        }
    }
}
