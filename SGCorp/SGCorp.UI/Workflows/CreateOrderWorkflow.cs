﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using SGCorp.BLL;
using SGCorp.Models;
using SGCorp.UI.Utilities;

namespace SGCorp.UI.Workflows
{
    class CreateOrderWorkflow : IWorkflow
    {
        private OrderManager orderManager;
        private ArrayList repoList;

        public CreateOrderWorkflow()
        {
            orderManager = new OrderManager();
        }

        public void Execute()
        {            
            State st = null;           
            Product p = null;
            int typeOfObject = 0;
            string error;

            Console.Write("Enter your name: ");
            string name = InputValidator.UseStringIfValid();

            repoList = orderManager.DisplayTypeList(typeOfObject); //get either state or product information

                do
                {
                    Console.Write("Enter your State: ");
                    string state = InputValidator.UseStringIfValid();

                    if(orderManager.IsSuccess)
                        st = CheckAvailableStates.GetState(state, repoList);

                    if(st == null) {
                    error = "Tax information cannot be retrieved from this state.  Please try again\n";
                    Console.Write(error);
                    }
                    else
                        break;                    
                } while (true);            

            typeOfObject++;
           
            repoList = orderManager.DisplayTypeList(typeOfObject);

                do
                {
                    Console.Write("Enter your flooring product type: ");
                    string productType = InputValidator.UseStringIfValid();

                    if (orderManager.IsSuccess)
                        p = CheckAvailableProducts.GetProduct(productType, repoList);

                    if(p == null) {
                    error = "There is no supplier that currently sells that item.  Please try again\n";
                    Console.Write(error);
                    }
                    else
                        break;                    
                } while (true);           

            Console.Write("Enter the calculated area of your product: ");
            string area;
            decimal decArea;

            do
                area = InputValidator.UseStringIfValid();
            while (!decimal.TryParse(area, out decArea));

            decimal materialsCost = 0;
            decimal laborCost = 0;
            decimal taxAmount = 0;
            decimal totalCost = 0;

                if (area != "")
                {
                    materialsCost = Calculations.CalculateMaterialsCost(decArea, p.CostPerSqFoot);
                    laborCost = Calculations.CalculateLaborCost(decArea, p.LaborPerSqFoot);
                    taxAmount = Calculations.CalculateTaxAmount(materialsCost, laborCost, st.TaxRate);
                    totalCost = Calculations.CalculateTotalCost(materialsCost, laborCost, taxAmount);
                }

            Console.Write("\nName: " + name + 
                          "\nState: " + st.StateAbbrev +
                          "\nState Tax Rate: " + st.TaxRate +
                          "\nProduct Type: " + p.ProductType +
                          "\nCost Per Square Foot: " + p.CostPerSqFoot +
                          "\nLabor Per Square Foot: " + p.LaborPerSqFoot +
                          "\nArea: " + area + 
                          "\nMaterials Cost: " + materialsCost + 
                          "\nLabor Cost: " + laborCost + 
                          "\nTax Amount: " + taxAmount + 
                          "\nTotal Cost: " + totalCost +
                          "\nOrder Date: " + DateTime.Now +
                          "\n\n");

            if(PromptUserIfSure.PromptToConfirm("Are you sure you want to add this order? (Y)es or (N)o"))
            {

            Order newOrder = new Order
            {
                CustomerName = name,
                StateName = st.StateName,
                StateAbbrev = st.StateAbbrev,
                TaxRate = st.TaxRate,
                ProductType = p.ProductType,
                CostPerSqFoot = p.CostPerSqFoot,
                LaborPerSqFoot = p.LaborPerSqFoot,
                Area = decArea,
                MaterialsCost = materialsCost,
                LaborCost = laborCost,
                TaxAmount = taxAmount,
                TotalCost = totalCost,
                DateOfOrder = DateTime.Now,
                IsActive = true,
                IsMoved = false,
            };

            orderManager.AddOrder(newOrder);             

            if (orderManager.IsSuccess) {
                Console.WriteLine("Order successfully created");
                Console.ReadLine();
            }
            else {
                Console.WriteLine("Order could not be created!");
                Console.ReadLine();
            }

            }
            
        }
    }
}
