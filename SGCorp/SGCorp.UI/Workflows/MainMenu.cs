﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCorp.UI.Workflows
{
    public class MainMenu
    {
        public static void Execute()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Welcome to SG Corp Flooring Program");
                Console.WriteLine("====================================");
                Console.WriteLine("1. Display Orders");
                Console.WriteLine("2. Add Order");
                Console.WriteLine("3. Edit Order");
                Console.WriteLine("4. Remove Order");
                Console.WriteLine("(Q) to Quit");

                Console.WriteLine("\nEnter an option to begin: ");

                string input = Console.ReadLine();              
                int intInput;

                if (!string.IsNullOrEmpty(input) && input.ToUpper() == "Q")
                    break;

                if (!int.TryParse(input, out intInput)) 
                    continue;

                if (intInput < 1 || intInput > 4)
                    continue;

                IWorkflow wfSelection = WorkflowFactory.WorkflowSelection(input);
                wfSelection.Execute();

            }
            while (true);
        }
    }
}

