﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.BLL;
using SGCorp.Models;
using SGCorp.UI.Utilities;

namespace SGCorp.UI.Workflows
{
    public class EditOrderWorkflow : IWorkflow
    {
        private OrderManager orderManager;
        private ArrayList repoList;

        public EditOrderWorkflow()
        {
            orderManager = new OrderManager();
        }

        public void Execute()
        {
            Console.Write("Enter date file that the order you wish to modify is in (MMddyyyy): ");
            DateTime date = StringToDate.StringToDateTime();

            List<Order> orderList = orderManager.DisplayAllOrders(date);
            DisplayDetails.DisplayAllOrderDetails(orderList);

            Order orderToModify;
            int intOrderId;
            decimal decArea;
            string orderId, area;
            State st;
            Product p;

            Console.Write("Enter the order ID you want to modify: ");

            do
            {
                orderId = InputValidator.UseStringIfValid();
                if (!int.TryParse(orderId, out intOrderId))
                    continue;

                orderToModify = orderManager.GetOrder(date, intOrderId);
                if (orderToModify == null)
                    Console.Write("There is no order ID in the file you specified! Try entering again: ");
                else if(!orderToModify.IsActive || orderToModify.IsMoved) {
                    Console.Write("This order was moved/deleted.  Press any key to continue...");
                    Console.ReadLine();
                    MainMenu.Execute();
                }
                else
                    break;
            } while (true);

            Console.Write("Enter Customer name (" + orderToModify.CustomerName + "): ");
            string name = EditInputValidator.CheckIfBlank();

            Console.Write("Enter a date for your order (" + orderToModify.DateOfOrder.ToString("MMddyyyy") + "): ");
            DateTime inputDate = EditInputValidator.CheckIfBlank(orderToModify.DateOfOrder);

            int typeOfObject = 0;

            Console.Write("Enter your state (" + orderToModify.StateAbbrev + "): ");
            string state = EditInputValidator.CheckIfBlank();

            repoList = orderManager.DisplayTypeList(typeOfObject);

            st = CheckAvailableStates.GetState(state, repoList);

            if (st == null)
            {
                st = new State();
                st.StateName = orderToModify.StateName;
                st.StateAbbrev = orderToModify.StateAbbrev;
                st.TaxRate = orderToModify.TaxRate;
            }

            Console.Write("Enter your product (" + orderToModify.ProductType + "): ");
            string productType = EditInputValidator.CheckIfBlank();

            typeOfObject++;

            repoList = orderManager.DisplayTypeList(typeOfObject);

            p = CheckAvailableProducts.GetProduct(productType, repoList);

            if (p == null)
            {
                p = new Product();
                p.ProductType = orderToModify.ProductType;
                p.CostPerSqFoot = orderToModify.CostPerSqFoot;
                p.LaborPerSqFoot = orderToModify.LaborPerSqFoot;
            }

            Console.Write("Enter the calculated area of your product (" + orderToModify.Area + "): ");

            area = EditInputValidator.CheckIfBlank();
                  
            if (area != " ")
            {
                decimal.TryParse(area, out decArea);
                decimal materialsCost = Calculations.CalculateMaterialsCost(decArea, p.CostPerSqFoot);
                decimal laborCost = Calculations.CalculateLaborCost(decArea, p.LaborPerSqFoot);
                decimal taxAmount = Calculations.CalculateTaxAmount(materialsCost, laborCost, st.TaxRate);
                decimal totalCost = Calculations.CalculateTotalCost(materialsCost, laborCost, taxAmount);

                orderToModify.MaterialsCost = materialsCost;
                orderToModify.LaborCost = laborCost;
                orderToModify.TaxAmount = taxAmount;
                orderToModify.TotalCost = totalCost;
            }
            else
                decArea = orderToModify.Area;

            if (name != " ")
            orderToModify.CustomerName = name;

            orderToModify.DateOfOrder = inputDate;
            orderToModify.StateName = st.StateName;            
            orderToModify.StateAbbrev = st.StateAbbrev;
            orderToModify.TaxRate = st.TaxRate;
            orderToModify.ProductType = p.ProductType;
            orderToModify.CostPerSqFoot = p.CostPerSqFoot;
            orderToModify.LaborPerSqFoot = p.LaborPerSqFoot;
            orderToModify.Area = decArea;

            orderManager.EditOrder(date, orderToModify);

            if(orderManager.IsSuccess) {
                Console.Write("Successfully modified order " + orderId + " in date file with date " + date.ToString("MM/dd/yyyy"));
                Console.ReadLine();
            }
            else
                Console.Write("Modify failed!");
        }


    }
}
