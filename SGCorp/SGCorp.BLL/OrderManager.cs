﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Data;
using SGCorp.Models;

namespace SGCorp.BLL
{
    public class OrderManager
    {
        private IOrderRepository repository;

        public bool IsSuccess { get; set; }
        public string Message { get; set; }

        public OrderManager()
        {
            repository = RepositoryFactory.RepositorySelection();
            IsSuccess = false;
        }
        
        public void WriteToFile(string ex)
        {
            try
            {
                repository.WriteToErrorFile(ex);
            }
            catch (Exception e)
            {
                Message = e.Message;
                IsSuccess = false;
            }
            IsSuccess = true;
        }

        public Order GetOrder(DateTime date, int orderId)
        {
            Order orderToModify = new Order();
            try
            {
                orderToModify = repository.LoadOrder(date, orderId);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                IsSuccess = false;
            }
            IsSuccess = true;

            return orderToModify;
        }

        public Order AddOrder(Order newOrder)
        {
            DateTime date = DateTime.Now;

            try
            {
                newOrder = repository.CreateOrder(date, newOrder);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                IsSuccess = false;
            }
            IsSuccess = true;

            return newOrder;
        }


        public Order EditOrder(DateTime date, Order order)
        {
            try
            {
                order = repository.EditOrder(date, order);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                IsSuccess = false;
            }
            IsSuccess = true;

            return order;
        }

        public void DeleteOrder(DateTime date, int orderId)
        {
            try
            {
                repository.RemoveOrder(date, orderId);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                IsSuccess = false;
            }
            IsSuccess = true;
        }

        public List<Order> DisplayAllOrders(DateTime date)
        {
            List<Order> displayOrders = new List<Order>();
            try
            {
                displayOrders = repository.DisplayOrders(date); //use the other interface

                if (displayOrders == null)
                {
                    IsSuccess = false;
                    return null;
                }
                IsSuccess = true;

            }
            catch (Exception ex)
            {
                Message = ex.Message;
                IsSuccess = false;
            }
            return displayOrders;
        }
        
        public ArrayList DisplayTypeList(int number)
        {
            ArrayList displayList = null;
            try
            {
                ITypeRepository repositoryType = TypeRepositoryFactory.GetTypeOfRepository(number);
                displayList = repositoryType.DisplayAvailableList();
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                IsSuccess = false;
            }
            IsSuccess = true;

            return displayList;
        }
    }
}

