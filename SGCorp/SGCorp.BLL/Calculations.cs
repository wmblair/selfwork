﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCorp.UI
{
    public static class Calculations
    {
        public static decimal CalculateMaterialsCost(decimal area, decimal costPerSqFoot)
        {
            return area*costPerSqFoot;
        }

        public static decimal CalculateLaborCost(decimal area, decimal laborPerSqFoot)
        {
            return area*laborPerSqFoot;
        }

        public static decimal CalculateTaxAmount(decimal materials, decimal laborCost, decimal taxRate)
        {
            return (materials + laborCost)*taxRate;
        }

        public static decimal CalculateTotalCost(decimal materials, decimal laborCost, decimal taxAmount)
        {
            return materials + laborCost + taxAmount;
        }
    }
}
