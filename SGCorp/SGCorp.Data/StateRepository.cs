﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Models;

namespace SGCorp.Data
{
    public class StateRepository : ITypeRepository
    {
        public ArrayList DisplayAvailableList()
        {
            ArrayList validTaxes = new ArrayList();
            var rowsInFile = File.ReadAllLines(@"DataFiles\Taxes.txt");

            for (int i = 1; i < rowsInFile.Length; i++)
            {
                var columnsInFile = rowsInFile[i].Split(',');

                State state = new State
                {
                    StateAbbrev = columnsInFile[0],
                    StateName = columnsInFile[1],
                    TaxRate = decimal.Parse(columnsInFile[2])
                };
                validTaxes.Add(state);
            }
            return validTaxes;
        }
    }
}
