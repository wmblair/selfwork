﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Models;

namespace SGCorp.Data
{
    public static class TypeRepositoryFactory
    {
        public static ITypeRepository GetTypeOfRepository(int number)
        {
            switch (number)
            {
                case 0:
                    return new StateRepository();
                case 1:
                    return new ProductRepository();
                default:
                    return new ProductRepository();
            }
        }
    }
}
