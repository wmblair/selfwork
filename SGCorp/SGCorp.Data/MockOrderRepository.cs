﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Models;

namespace SGCorp.Data
{
    public class MockOrderRepository : IOrderRepository
    {
        private List<Order> list;
        private List<State> stateList;
        private List<Product> productList;

        public MockOrderRepository()
        {
            list = new List<Order>();
            stateList = new List<State>();
            productList = new List<Product>();

            stateList.Add(new State
            {
                StateName = "Ohio",
                StateAbbrev = "OH",
                TaxRate = 6.25m,
            });
            stateList.Add(new State
            {
                StateName = "Pennsylvania",
                StateAbbrev = "PA",
                TaxRate = 6.75m,
            });
            stateList.Add(new State
            {
                StateName = "Michigan",
                StateAbbrev = "MI",
                TaxRate = 5.75m,
            });
            stateList.Add(new State
            {
                StateName = "Indiana",
                StateAbbrev = "IN",
                TaxRate = 6.00m,
            });

            productList.Add(new Product
            {
                ProductType = "Carpet",
                CostPerSqFoot = 2.25m,
                LaborPerSqFoot = 2.10m
            });
            productList.Add(new Product
            {
                ProductType = "Laminate",
                CostPerSqFoot = 1.75m,
                LaborPerSqFoot = 2.10m
            });
            productList.Add(new Product
            {
                ProductType = "Tile",
                CostPerSqFoot = 3.50m,
                LaborPerSqFoot = 4.15m
            });
            productList.Add(new Product
            {
                ProductType = "Wood",
                CostPerSqFoot = 5.15m,
                LaborPerSqFoot = 4.75m
            });
        }

        public void WriteToErrorFile(string ex)
        {
        }

        public Order LoadOrder(DateTime date, int orderId)
        {
            list = DisplayOrders(date);
            return list.FirstOrDefault(a => a.OrderId == orderId);
        }

        public List<Order> DisplayOrders(DateTime date)
        {
            return list.Where(o => o.DateOfOrder == date).ToList();
        }

        public Order CreateOrder(DateTime date, Order order)
        {
            list = DisplayOrders(date);
            if (list.Count == 0)
                order.OrderId = 1;
            else
                order.OrderId = list.Select(o => o.OrderId).Max() + 1;
            list.Add(order);

            return LoadOrder(date, order.OrderId);
        }

        public Order EditOrder(DateTime date, Order order)
        {
            list = DisplayOrders(date); 
            var orderToEdit = list.FirstOrDefault(o => o.OrderId == order.OrderId);
            list.RemoveAt(order.OrderId-1);
            list.Insert(order.OrderId-1, order);

            return LoadOrder(date, order.OrderId);
        }

        public void RemoveOrder(DateTime date, int orderId)
        {
            list = DisplayOrders(date);
            var orderToRemove = list.First(o => o.OrderId == orderId);
            list.RemoveAt(orderId+1);
        }
    }
}
