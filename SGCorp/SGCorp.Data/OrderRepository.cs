﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Models;

namespace SGCorp.Data
{
    public class OrderRepository : IOrderRepository
    {
        private string path = @"log\errors.txt";

        public void WriteToErrorFile(string ex)
        {

            using (StreamWriter writeToErrorFile = new StreamWriter(@"log\errors.txt", true))//File.CreateText(path))
            {
                writeToErrorFile.WriteLine(ex); //ex.ToString();
            }
        }

        public Order LoadOrder(DateTime date, int orderId)
        {
            List<Order> list = DisplayOrders(date);
            return list.FirstOrDefault(a => a.OrderId == orderId);          
        }

        public List<Order> DisplayOrders(DateTime date)
        {
            List<Order> list = new List<Order>();

            string s = date.ToString("MMddyyyy");

            if (!File.Exists(GetTextFile(date)))
                return null;

            var rowsInFile = File.ReadAllLines(GetTextFile(date));

            if(rowsInFile.Length > 1)
            {
                for (int i = 1; i < rowsInFile.Length; i++)
                {
                    var columnsInFile = rowsInFile[i].Split(',');
                    int j = columnsInFile.Length-1;
                    
                    Order order = new Order {

                        OrderId = int.Parse(columnsInFile[columnsInFile.Length-1-j]), //use tryparse
                        CustomerName = columnsInFile[columnsInFile.Length-1-(--j)],
                        StateName = columnsInFile[columnsInFile.Length-1-(--j)],
                        StateAbbrev = columnsInFile[columnsInFile.Length-1-(--j)],
                        TaxRate = decimal.Parse(columnsInFile[columnsInFile.Length-1-(--j)]), //order.TaxRate =
                        ProductType = columnsInFile[columnsInFile.Length-1-(--j)],
                        Area = decimal.Parse(columnsInFile[columnsInFile.Length-1-(--j)]),
                        CostPerSqFoot = decimal.Parse(columnsInFile[columnsInFile.Length-1-(--j)]),
                        LaborPerSqFoot = decimal.Parse(columnsInFile[columnsInFile.Length-1-(--j)]),
                        MaterialsCost = decimal.Parse(columnsInFile[columnsInFile.Length-1-(--j)]),
                        LaborCost = decimal.Parse(columnsInFile[columnsInFile.Length-1-(--j)]),
                        TaxAmount = decimal.Parse(columnsInFile[columnsInFile.Length-1-(--j)]),
                        TotalCost = decimal.Parse(columnsInFile[columnsInFile.Length-1-(--j)]),
                        DateOfOrder = DateTime.ParseExact(columnsInFile[columnsInFile.Length-1-(--j)], "MM/dd/yyyy", CultureInfo.InvariantCulture),
                    };

                    if (columnsInFile[columnsInFile.Length - 1 - (--j)] == "")
                        order.IsActive = true;
                    else
                        order.IsActive = false;

                    if (columnsInFile[columnsInFile.Length - 1 - (--j)] == "")
                        order.IsMoved = false;
                    else
                        order.IsMoved = true;                      
            
                    list.Add(order);
                }
            }                     
            return list;
        }

        public Order EditOrder(DateTime date, Order order)
        {
            if (!File.Exists(GetTextFile(date)) || !File.Exists(GetTextFile(order.DateOfOrder)))
                return null;

            if (order.DateOfOrder == date) {     //just edit the order in place
                List<Order> list = DisplayOrders(date);             
                var orderToModify = list.FirstOrDefault(o => o.OrderId == order.OrderId);

                list.RemoveAt(order.OrderId-1);
                list.Insert(order.OrderId-1, order);            
                WriteToFile(list, date);
            }

            else if (!order.IsMoved && order.DateOfOrder != date) //if you specify a new date file for the current order
            {
                List<Order> list = DisplayOrders(date);

                Order newOrder = new Order { DateOfOrder = order.DateOfOrder};
                //store the new date somewhere

                order.DateOfOrder = date;
                list.RemoveAt(order.OrderId-1); //remove the old order in the list so that the "moved" setting can be updated

                order.IsMoved = true;
                order.IsActive = true;

                list.Insert(order.OrderId-1, order);   

                WriteToFile(list, date);
                order.IsMoved = false;

                CreateOrder(newOrder.DateOfOrder, order); //create the record in the file you want to move it to
            }
            return LoadOrder(date, order.OrderId); //return the order in the original location regardless
        }

        public void RemoveOrder(DateTime date, int orderId)
        {
            List<Order> list = DisplayOrders(date);
            var orderToRemove = list.First(o => o.OrderId == orderId);
            Order order = orderToRemove;
            order.IsActive = false;

            WriteToFile(list, date);
        }

        public Order CreateOrder(DateTime date, Order order)
        {
            if (!File.Exists(GetTextFile(date)))
                File.Create(GetTextFile(date));

            List<Order> list = DisplayOrders(date);

            if (list.Count == 0)
                order.OrderId = 1;
            else
                order.OrderId = list.Select(o => o.OrderId).Max() + 1;

            order.DateOfOrder = date;

            list.Add(order);
            WriteToFile(list, date);
            return LoadOrder(date, order.OrderId);
        }

        public void WriteToFile(List<Order> list, DateTime date)
        {
            if (File.Exists(GetTextFile(date)))
               File.Delete(GetTextFile(date));

            using (StreamWriter writeToNewFile = File.CreateText(GetTextFile(date)))
            {
                writeToNewFile.WriteLine("Order Id, Customer Name, State Name, State Abbreviation, State Tax Rate, Product Type, Area, Cost Per Sq Foot, Labor Per Sq Foot, Materials Cost, Labor Cost, Tax Amount, Total Cost");

                foreach (Order order in list)
                {
                    writeToNewFile.WriteLine(order.OrderId + ","
                                             + order.CustomerName + ","
                                             + order.StateName + ","
                                             + order.StateAbbrev + ","
                                             + order.TaxRate + ","
                                             + order.ProductType + ","
                                             + order.Area + ","
                                             + order.CostPerSqFoot + ","
                                             + order.LaborPerSqFoot + ","
                                             + order.MaterialsCost + ","
                                             + order.LaborCost + ","
                                             + order.TaxAmount + ","
                                             + order.TotalCost + ","
                                             + order.DateOfOrder.ToString("MM/dd/yyyy") + ","
                                             + (order.IsActive ? "" : "(deleted)") + ","
                                             + (order.IsMoved && order.IsActive ? "(moved)" : ""));                
                }                           
            }
        }

        public string GetTextFile(DateTime date)
        {
            string path = @"DataFiles\Orders_" + date.ToString("MMddyyyy") + ".txt";
            return path;
        }
    }
}
