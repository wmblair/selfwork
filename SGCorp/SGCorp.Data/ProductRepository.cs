﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Models;

namespace SGCorp.Data
{
    public class ProductRepository : ITypeRepository
    {
        public ArrayList DisplayAvailableList()
        {
            ArrayList validProducts = new ArrayList();    
            var rowsInFile = File.ReadAllLines(@"DataFiles\Products.txt");

            for (int i = 1; i < rowsInFile.Length; i++)
            {
                var columnsInFile = rowsInFile[i].Split(',');
                Product product = new Product();
                product.ProductType = columnsInFile[0];
                product.CostPerSqFoot = decimal.Parse(columnsInFile[1]);
                product.LaborPerSqFoot = decimal.Parse(columnsInFile[2]);
                validProducts.Add(product);
            }
            return validProducts;
        }
    }
}
