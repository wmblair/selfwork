﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCorp.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public string CustomerName { get; set; }
        public decimal MaterialsCost { get; set; }
        public decimal LaborCost { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Area { get; set; }
        public string ProductType { get; set; }
        public decimal CostPerSqFoot { get; set; }
        public decimal LaborPerSqFoot { get; set; }
        public string StateName { get; set; }
        public string StateAbbrev { get; set; }
        public decimal TaxRate { get; set; }
        public DateTime DateOfOrder { get; set; }
        public bool IsActive { get; set; }
        public bool IsMoved { get; set; }
    }
}
