using System;
using System.Media;
using System.Security.Policy;

namespace TicTacToe.BLL.GameLogic
{
    public class TicTacToeGame
    {

        public void PlayGame()
        {
            Console.WriteLine("Welcome to Tic Tac Toe!");
            string p1 = "", p2 = "";
            ReadNames(ref p1, ref p2);

            char[,] placementOptions = new char[3,3];

            SetupBoard(placementOptions);

            int i = 0, x = 0, y = 0;
            bool win = false, tie = false;

            do
            {
                if(i % 2 == 0)
                Console.WriteLine("\n" + p1 + "'s turn. ");
                else
                Console.WriteLine("\n" + p2 + "'s turn. ");

                Console.Write("Type an [X,Y] coordinate to place on the board (zero-based): \n");

                PrintBoard(placementOptions);

                string move = Console.ReadLine();

                if(ParsePosition(move, ref x, ref y)) {

                    if (x <= 2 && y <= 2)
                    {
                        if (placementOptions[x, y] == ' ')
                        {
                            placementOptions = PlaceMove(x, y, placementOptions, i);

                            if(CheckIfWin(placementOptions) || CheckIfTie(placementOptions))
                                break;

                            i = (i + 1) % 2;
                            Console.Clear();
                            continue;
                        }
                            Console.Clear();
                            Console.Write("That coordinate is already taken! Please try again");
                            i++;
                            i = (i + 1) % 2;
                            continue;                      
                    }
                }
                    Console.Clear();                
                    Console.Write("Please enter a coordinate in the form of \"number,number\" and number < 3: ");                    
                    i++;
                    i = (i + 1) % 2;
            }
            while (!win || !tie);

            Console.Clear();

            if (CheckIfWin(placementOptions))
            {
                if(i % 2 == 0)
                    Console.Write("\nCongratulations " + p1 + "!  You just won!\n\n");
                else
                    Console.Write("\nCongratulations " + p2 + "!  You just won!\n\n");
            }
            else
                Console.Write("\nBoth players tied the game!\n\n");
            
            PrintBoard(placementOptions);

            Console.WriteLine("\nPlay again? Enter Y for yes or any key to quit: ");
            string playOrQuit = Console.ReadLine();

            if (playOrQuit.ToUpper() == "Y")
            {
                Console.Clear();
                PlayGame();
            }
        }
        
        #region placemove
        static char[,] PlaceMove(int x, int y, char[,] s, int turn)
        {
            if(turn % 2 == 0)
                    s[x, y] = 'X';         
            else
                    s[x, y] = 'O';
            return s;
        }
        #endregion

        #region readnames
        static void ReadNames(ref string player1, ref string player2)
        {
            Console.WriteLine("Enter a name for Player 1: ");
            player1 = Console.ReadLine();
            
            Console.WriteLine("Enter a name for Player 2: ");
            player2 = Console.ReadLine();
            Console.Clear();
        }
        #endregion

        #region printboard
        static void PrintBoard(char[,] theBoard)
        {
            for (int i = 0; i < 3; i++)
            {
                if (i != 0)
                    Console.WriteLine("-----------------");
                for (int j = 0; j < 3; j++)
                {
                    if (j == 1)
                    Console.WriteLine("  " + theBoard[i,0] + "  |  " + theBoard[i,1] + "  |  " + theBoard[i,2] + "  ");
                    else
                    Console.WriteLine("     |     |     ");
                }
            }
        }
        #endregion

        static bool ParsePosition(string input, ref int x, ref int y)
        {
            if (input.Length <= 3 && input.Length > 1 && input[1] == ',')
            {
                x = int.Parse(input[0].ToString());
                y = int.Parse(input[2].ToString());
                return true;
            }
            return false;
        }
        /*
        public int ParseYPosition(string input)
        {
            int y = 0;
            if (input.Length <= 3 && input.Length > 1 && input[1] == ',')
                y = int.Parse(input[2].ToString());           
            return y;
        }
        */

        #region checkiftie
        static bool CheckIfTie(char [,] s)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++) {
                    if (s[i, j] == ' ')
                        return false;
                }
            }
            if (!CheckIfWin(s))
                return true;
            return false;
        }
        #endregion

        #region setupboard
        static void SetupBoard(char [,] s)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                    s[i, j] = ' ';
            }
        }
        #endregion

        #region checkifwin
        static bool CheckIfWin(char [,] s)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (s[i, 0] != ' ' && s[i, 0] == s[i, 1] && s[i, 1] == s[i, 2]) //check rows
                        return true;
                    if (s[0, j] != ' ' && s[0, j] == s[1, j] && s[1, j] == s[2, j]) //check columns
                        return true;
                }
            }
            if (s[0, 0] != ' ' && s[0, 0] == s[1, 1] && s[1, 1] == s[2, 2]) //check 1 of 2 diagonals
                return true;
            if (s[0, 2] != ' ' && s[0, 2] == s[1, 1] && s[1, 1] == s[2, 0]) //check 2 of 2 diagonals
                return true;

            return false;
        }
        #endregion

    }
}
