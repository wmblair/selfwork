﻿using TicTacToe.BLL.GameLogic;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            TicTacToeGame ticTacToe = new TicTacToeGame();
            ticTacToe.PlayGame();
        }
    }
}
