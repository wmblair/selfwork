﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelBindingSample.Models;

namespace ModelBindingSample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddPerson()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddPersonForm()
        {
            return View(new Person());
        }

        [HttpGet]
        public ActionResult AddPersonForm2()
        {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult AddPersonForm2(Person person)
        {
            return View("AddPerson", person);
        }

        [HttpGet]
        public ActionResult BindComplexClassTypes()
        {
            return View(new Person() {Address = new Address()});
        }

        [HttpPost]
        public ActionResult AddPersonFormResult()
        {
            var model = new Person();
            model.PersonId = int.Parse(Request.Form["PersonId"]);
            model.FirstName = Request.Form["FirstName"];
            model.LastName = Request.Form["LastName"];

            return View("AddPerson", model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}