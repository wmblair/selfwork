﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ModelBindingSample.Models
{
    public class Address
    {
        public string City { get; set; }
        public string State { get; set; }
        public int? Zip { get; set; }
    }
}