﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            SimpleArrays();
            RectMultiDimensionalArray();
        }

        static void SimpleArrays()
        {
            Console.WriteLine("=> Simple Arrays");

            int[] myInts = new int[3];
            myInts[0] = 100;
            myInts[1] = 200;
            myInts[2] = 300;

            foreach (int num in myInts)
            {
                Console.WriteLine(num);
            }
            Console.ReadLine();
        }

       // static void ArrayInitialization()
        //{
            
       // }

        static void RectMultiDimensionalArray()
        {
            Console.Clear();
            Console.WriteLine("=> Rect Multidimensional array");

            int[,] myMatrix;
            myMatrix = new int[6, 6];

            for (int i = 0; i < 6; i++)
                for (int j = 0; j < 6; j++)
                    myMatrix[i, j] = i*j;
                //this line is unreachable by outer for loop

            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 6; j++)
                    Console.Write(myMatrix[i, j] + "\t");
            Console.WriteLine();
            }

        Console.ReadLine();
        }
    }
}
