﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarmupLabStringLoops
{
    public class Loops
    {
        public string StringTimes(string str, int n)
        {
        StringBuilder sb = new StringBuilder();
            for (int i = 0; i < n; i++)
            {
                sb.Append(str);
            }
            return sb.ToString();
        }

        public string FrontTimes(string str, int n)
        {
            StringBuilder sb = new StringBuilder();

            if (str.Length >= 3)
            {
                for (int i = 0; i < n; i++)
                {
                    sb.Append(str.Substring(0,3));                  
                }
                return sb.ToString();
            }
            for (int i = 0; i < n; i++)
            {
                sb.Append(str);
            }
            return sb.ToString();
        }
        public int CountXX(string str)
        {
            int counter = 0;

            for (int i = 0; i < str.Length-1; i++)
            {
                if (str.Substring(i, 2) == "xx")
                {
                    counter++;
                }
            }
            return counter;
        }

        public bool DoubleX(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == 'x' && str[i+1] == 'x')
                {
                    return true;
                }
                if (str[i] == 'x' && str[i + 1] != 'x')
                {
                    return false;
                }
            }
            return false;
        }
        public string EveryOther(string str)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < str.Length; i+=2)
            {
                sb.Append(str[i]);
            }
            return sb.ToString();
        }

        public string StringSplosion(string str)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < str.Length; i++)
            {
                sb.Append(str.Substring(0, i+1));
            }
            return sb.ToString();
        }

        public int CountLast2(string str)
        {
            int counter = 0;

            for (int i = 0; i < str.Length-3; i++)
            {
                if (str[i] + str[i+1] == str[str.Length-2] + str[str.Length-1])
                {
                    counter++;
                }
            }
            return counter;
        }

        public int Count9(int[] numbers)
        {
            int counter = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] == 9)
                {
                    counter++;
                }
            }
            return counter;
        }

        public bool ArrayFront9(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                if (i < 4 && numbers[i] == 9)
                {
                    return true;
                }
            }
            return false;
        }

        public bool Array123(int[] numbers)
        {
            for (int i = 0; i < numbers.Length-2; i++)
            {
                if (numbers[i] == 1)
                {
                    if ((numbers[i+1] == numbers[i]+1) && (numbers[i+2] == numbers[i]+2))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public int SubStringMatch(string a, string b)
        {
            int size = 0;
            int count = 0;

            if (a.Length > b.Length)
                size = b.Length;
            else
            size = a.Length;

            for (int i = 0; i < size-1; i++)
            {
                if (a.Substring(i, 2) == b.Substring(i, 2))
                    count++;               
            }
            return count;
        }

        public string StringX(string str)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(str);
            sb = sb.Replace("x", "");
            if (str[0] == 'x')
                sb.Insert(0,'x');
            if (str[str.Length-1] == 'x')
                sb.Append('x');
            return sb.ToString();
        }

        public string AltPairs(string str)
        {
            string sub = "";
            for (int i = 0; i < str.Length; i+=4)
            {
                if (i == str.Length-1)
                    sub += str[str.Length - 1];
                else
                    sub += str.Substring(i, 2);
            }
            return sub;
        }

        public string DoNotYak(string str)
        {
            for (int i = 0; i < str.Length-2; i++)
            {
                if (str.Substring(i,3) == "yak")
                    str = str.Remove(i,3);
            }
            return str;
        }

        public int Array667(int[] numbers)
        {
            int count = 0;
            for (int i = 0; i < numbers.Length-1; i++)
            {
                if ((numbers[i] == 6 && numbers[i] == numbers[i+1]) || (numbers[i] == 6 && numbers[i] == numbers[i+1]-1))
                    count++;               
            }
            return count;
        }

        public bool NoTriples(int[] numbers)
        {
            for (int i = 0; i < numbers.Length-2; i++)
            {
                if (numbers[i] == numbers[i+1] && numbers[i+1] == numbers[i+2])
                    return false;
            }
            return true;
        }

        public bool Pattern51(int[] numbers)
        {
            for (int i = 0; i < numbers.Length-2; i++)
            {
                if (numbers[i+1] == numbers[i]+5 && numbers[i+2] == numbers[i+1]-6)
                    return true;
            }
            return false;
        }
    }

}
