﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WarmupLabStringLoops;

namespace WarmupLabStringLoopTests
{
    [TestFixture]
    public class LoopTests
    {
        private Loops _stringloops;

        [SetUp]
        public void BeforeEachTest()
        {
            _stringloops = new Loops();
        }

        [TestCase("Hi", 3, "HiHiHi")]
        public void StringTimesReturnsExpected(string input, int n, string expected)
        {
            string result = _stringloops.StringTimes(input, n);
            Assert.AreEqual(expected, result);
        }

        [TestCase("Chocolate", 5, "ChoChoChoChoCho")]
        [TestCase("Pi", 4, "PiPiPiPi")]
        public void FrontTimesReturnsExpected(string input, int n, string expected)
        {
            string result = _stringloops.FrontTimes(input, n);
            Assert.AreEqual(expected, result);
        }

        [TestCase("xxxx", 3)]
        [TestCase("abcxx", 1)]
        public void CountXXReturnsExpected(string input, int expected)
        {
            int result = _stringloops.CountXX(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("xxxx", true)]
        [TestCase("abxcxx", false)]
        [TestCase("dsdafxxfsfaxsfa", true)]
        [TestCase("gsdgxdxxfsddf", false)]
        public void DoubleXReturnsExpected(string input, bool expected)
        {
            bool result = _stringloops.DoubleX(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", "Hlo")]
        [TestCase("hi", "h")]
        [TestCase("Heeololeo", "Hello")]
        public void EveryOtherReturnsExpected(string input, string expected)
        {
            string result = _stringloops.EveryOther(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", "HHeHelHellHello")]
        [TestCase("Code", "CCoCodCode")]
        public void StringSplosionReturnExpected(string input, string expected)
        {
            string result = _stringloops.StringSplosion(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("hixxhi", 1)]
        [TestCase("xaxxaxaxx", 1)]
        [TestCase("axxxaaxx", 2)]
        public void CountLast2ReturnsExpected(string input, int expected)
        {
            int result = _stringloops.CountLast2(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] {1, 2, 9, 5, 9}, 2)]
        [TestCase(new int[] {9, 9, 9, 3, 9, 2, 9}, 5)]
        public void Count9ReturnsExpected(int[] input, int expected)
        {
            int result = _stringloops.Count9(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] { 1, 9, 9, 5, 9 }, true)]
        [TestCase(new int[] {9}, true)]
        [TestCase(new int[] {1, 2, 3, 4, 9}, false)]
        public void ArrayFront9ReturnsExpected(int[] input, bool expected)
        {
            bool result = _stringloops.ArrayFront9(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] {1,1,2,3,1}, true)]
        [TestCase(new int[] {1,1,2,4,1}, false)]
        [TestCase(new int[] {1,1,2,1,2,3}, true)]
        public void Array123ReturnsExpected(int[] input, bool expected)
        {
            bool result = _stringloops.Array123(input);
            Assert.AreEqual(expected, result);
        }
        //11-17

        [TestCase("xxcaazz", "xxbaaz", 3)]
        [TestCase("abc", "abc", 2)]
        [TestCase("defg", "defg", 3)]
        [TestCase("abc", "axc", 0)]
        public void SubStringMatchReturnsExpected(string a, string b, int expected) {
            int result = _stringloops.SubStringMatch(a,b);
            Assert.AreEqual(expected, result);
        }

        [TestCase("xxHxix", "xHix")]
        [TestCase("abxxxcd", "abcd")]
        [TestCase("xabxxxcdx", "xabcdx")]
        public void StringXReturnsExpected(string input, string expected)
        {
            string result = _stringloops.StringX(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("kitten", "kien")]
        [TestCase("Chocolate", "Chole")]
        [TestCase("Codinghorror", "Congrr")]
        public void AltPairsReturnsExpected(string input, string expected) {
            string result = _stringloops.AltPairs(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("yakpak", "pak")]
        [TestCase("pakyak", "pak")]
        [TestCase("yak123ya", "123ya")]
        public void DoNotYakReturnsExpected(string input, string expected) {
            string result = _stringloops.DoNotYak(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] {1, 6, 6, 7, 6}, 2)]
        [TestCase(new int[] {9, 7, 6, 7, 6, 6, 7}, 3)]
        public void Array667ReturnsExpected(int[] input, int expected) {
            int result = _stringloops.Array667(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] {1, 1, 2, 2, 1}, true)]
        [TestCase(new int[] {1, 1, 2, 2, 2, 1}, false)]
        [TestCase(new int[] {1, 1, 1, 2, 2, 2, 1}, false)]
        public void NoTriplesReturnsExpected(int[] input, bool expected) {
            bool result = _stringloops.NoTriples(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase(new int[] {1, 2, 7, 1}, true)]
        [TestCase(new int[] {1, 2, 8, 1}, false)]
        [TestCase(new int[] {2, 7, 1}, true)]
        public void Pattern51ReturnsExpected(int[] input, bool expected) {
            bool result = _stringloops.Pattern51(input);
            Assert.AreEqual(expected, result);
        }


    }
}
