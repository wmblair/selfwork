﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SGCorpHRSite.Startup))]
namespace SGCorpHRSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
