﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DVDLibrary.Config;
using DVDLibrary.Models;
using DVDLibrary.Models.View_Models;
using Dapper;

namespace DVDLibrary.Repositories
{
    public class DvdRepository
    {
        public Dvd GetDvdById(int id)
        {
            Dvd dvd = new Dvd();

            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "select * from Dvd where Dvd.DVDid = @DvdId";

                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@DvdId", id);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dvd = PopulateDvdFromDataReader(dr);
                    }
                }
            }
            return dvd;
        }

        public List<BorrowerViewModel> DisplayBorrowersForDvd(int dvdid)
        {
            List<BorrowerViewModel> searchList;

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var results = cn.Query<BorrowerViewModel, DvdBorrower, Borrower, BorrowerViewModel>
                    ("DisplayBorrowersForDvd", (v, db, b) =>
                    {
                        v.DvdBorrower = db;
                        v.Borrower = b;
                        return v;
                    }, 
                new {dvdid},
                commandType: CommandType.StoredProcedure, 
                splitOn: "DvdId,DvdBorrowerId,BorrowerId").ToList();
                searchList = results;
            }
            return searchList;

        }

        public List<DvdViewModel> GetAllDvds()
        {
            List<DvdViewModel> dvds = new List<DvdViewModel>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                           
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select Dvd.*, MpaaRating.MpaaRatingName, Studio.StudioName from Dvd " +
                                  "inner join MpaaRating on Dvd.MpaaRatingId = MpaaRating.MpaaRatingId " + 
                                  "inner join Studio on Dvd.StudioId = Studio.StudioId";

                cmd.Connection = cn;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read()) //goes through one record per iteration
                    {
                        dvds.Add(PopulateDvdViewModelFromDataReader(dr));
                    }
                }               
            }
            
            return dvds;
        }

        public void RemoveDvd(int id)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                //select the studio associated with this dvd
                Studio dvdToRemove = cn.Query<Studio>("Select * from Studio inner join Dvd " +
                                                   "on Studio.Studioid = Dvd.Studioid where DvdId=@id", new {id}).First();

                //delete the dvd
                cn.Query("Delete from DVD where DvdId=@id", new {id});

                //check if there is only one studio that was in the deleted dvd in the result
                if (cn.Query<Studio>("Select * from Studio where Studio.StudioId = @studioId", new {studioId = dvdToRemove.StudioId}).Count() == 1)
                    cn.Query("Delete from Studio where StudioId = @studioId", new {studioId = dvdToRemove.StudioId});
            }
        }

        public DvdViewModel AddNewDvd(DvdViewModel dvdvm)
        {
            var query = "Select StudioId from Studio where StudioName = @StudioName";

            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                using (var cmd = new SqlCommand(query, cn))
                {
                    cn.Open();

                    DateTime rngMin = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                    DateTime rngMax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;
                    
                    if (dvdvm.Studio.StudioName == null || dvdvm.Dvd.Title == null 
                        || dvdvm.MpaaRating.MpaaRatingId == 0)
                        return null;
                    if (dvdvm.Dvd.ReleaseDate < rngMin || dvdvm.Dvd.ReleaseDate > rngMax)
                        return null;

                        cmd.Parameters.AddWithValue("@StudioName", dvdvm.Studio.StudioName);
                        object result = cmd.ExecuteScalar();
                    
                        if (result != null)         //if a studio name already exists for the input, just set dvd's studio id to the single value returned
                            dvdvm.Studio.StudioId = Convert.ToInt32(result.ToString());
                        else                        //if it doesn't you need to insert the new studio AND run previous query again to get the id of the new studio
                        {
                            cmd.CommandText = "Insert into Studio values (@StudioName)";
                            cmd.ExecuteNonQuery();

                            cmd.CommandText = "Select StudioId from Studio where StudioName = @StudioName";
                            dvdvm.Studio.StudioId = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                        }

                    cn.Close();
                }
                    
                    var p = new DynamicParameters();
                    p.Add("ReleaseDate", dvdvm.Dvd.ReleaseDate); //the value you're assigning to the column
                    p.Add("Title", dvdvm.Dvd.Title);
                    p.Add("StudioId", dvdvm.Studio.StudioId);
                    p.Add("MpaaRatingID", dvdvm.MpaaRating.MpaaRatingId);
                    p.Add("UnitsInStock", dvdvm.Dvd.UnitsInStock);                                                   
                    p.Add("Link", dvdvm.Dvd.Link);    

                    cn.Execute("AddNewDvd", p, commandType: CommandType.StoredProcedure);                     
            }
            return dvdvm;
        }

        public string GetRatingNameFromId(int id)
        {
            var query = "select MPAARatingName from MPAARating where MPAARatingId = @MMPARatingId";
            string ratingName;

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                using (var cmd = new SqlCommand(query, cn))
                {
                    cn.Open();
                    ratingName = cmd.ExecuteScalar().ToString();
                }
            }
            return ratingName;
        }

        public List<DvdViewModel> SearchForDvds(string title)
        {
            List<DvdViewModel> searchList;

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {

                var results = cn.Query<DvdViewModel, Dvd, Studio, MpaaRating, DvdViewModel>
                    ("SearchQuery", (v, d, s, m) =>
                    {
                        v.Dvd = d;
                        v.Studio = s;
                        v.MpaaRating = m;
                        return v;
                    }, 
                new {title},
                commandType: CommandType.StoredProcedure, 
                splitOn: "DvdId,StudioId,MPAARatingId").ToList();
                //if primary keys aren't named "id", use splitOn
                searchList = results;
            }
            return searchList;
        }
        
        public List<MpaaRating> GetAllRatings()
        {
            List<MpaaRating> ratingsList;

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var ratings = cn.Query<MpaaRating>("Select * from MPAARating").ToList();

                ratingsList = ratings;
            }
            return ratingsList;
        }

        private DvdViewModel PopulateDvdViewModelFromDataReader(SqlDataReader dr)           
        {
            DvdViewModel dvdvm = new DvdViewModel();
            dvdvm.Dvd = new Dvd();
            dvdvm.MpaaRating = new MpaaRating();
            dvdvm.Studio = new Studio();

            dvdvm.Dvd.DvdId = (int) dr["DVDId"];

            if (dr["ReleaseDate"] != DBNull.Value ) 
                dvdvm.Dvd.ReleaseDate = (DateTime)dr["ReleaseDate"];           

            if (dr["Title"] != DBNull.Value)
                dvdvm.Dvd.Title = dr["Title"].ToString();

            dvdvm.Studio.StudioId = (int) dr["StudioId"];

            dvdvm.Studio.StudioName = dr["StudioName"].ToString();

            dvdvm.MpaaRating.MpaaRatingId = (int) dr["MPAARatingId"];

            if (dr["UnitsInStock"] != DBNull.Value)
                dvdvm.Dvd.UnitsInStock = (int) dr["UnitsInStock"];

            dvdvm.MpaaRating.MpaaRatingName = dr["MPAARatingName"].ToString();

            dvdvm.Dvd.Link = dr["Link"].ToString();

            return dvdvm;
        }

        private Dvd PopulateDvdFromDataReader(SqlDataReader dr) //map the columns SQLDataReader found to a new Dvd object
        {
            Dvd dvd = new Dvd();

            dvd.DvdId = (int) dr["DVDId"];

            if (dr["ReleaseDate"] != DBNull.Value ) 
                dvd.ReleaseDate = (DateTime)dr["ReleaseDate"];           

            if (dr["Title"] != DBNull.Value)
                dvd.Title = dr["Title"].ToString();

            if (dr["UnitsInStock"] != DBNull.Value)
                dvd.UnitsInStock = (int) dr["UnitsInStock"];

            return dvd;
        }

    }
}