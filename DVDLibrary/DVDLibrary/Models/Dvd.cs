﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DVDLibrary.Models.Annotations;

namespace DVDLibrary.Models
{
    public class Dvd //had populate ratingslist selectlist function member
    {   //could pass Dvd itself into the view, didn't have to use view model
        //originally had it set up where it mirrored database tables with foreign keys (ids)

        public int DvdId { get; set; }

        [Display(Name="Release Date")]
        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }

        //[Required(ErrorMessage = "Please enter a title for the movie")]
        public string Title { get; set; }

       // public int StudioId { get; set; }

      //  public int MpaaRatingId { get; set; }

        //    [Display(Name="Studio Name")]
    //    [Required(ErrorMessage = "Please enter a studio that produced this movie")]
    //    public string StudioName { get; set; }

        [Display(Name="Units In Stock")]
        public int? UnitsInStock { get; set; }

        public string Link { get; set; }
    }
}