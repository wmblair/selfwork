﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVDLibrary.Models.View_Models
{
    public class BorrowerViewModel
    {
        //dvdborrower, borrower, dvd
        public Dvd Dvd { get; set; }
        public DvdBorrower DvdBorrower { get; set; }
        public Borrower Borrower { get; set; }
    }
}