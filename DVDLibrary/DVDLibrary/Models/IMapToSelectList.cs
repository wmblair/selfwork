﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DVDLibrary.Models
{
    interface IMapToSelectList
    {
        List<SelectListItem> ItemsList { get; set; }

        /*
        SetListItems(IEnumerable<Studio> studios)
        {
            foreach (var studio in studios)
            {
                StudioItems.Add(new SelectListItem()
                {
                    Value = studio.StudioId.ToString(),
                    Text = studio.StudioName
                });
            }
        }
        */
    }
}
