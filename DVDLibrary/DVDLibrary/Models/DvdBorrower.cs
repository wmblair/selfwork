﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DVDLibrary.Models
{
    public class DvdBorrower
    {
        public int DvdBorrowerId { get; set; }

        [Display(Name="Time Start")]
        [DataType(DataType.Date)]
        public DateTime TimeStart { get; set; }

        [Display(Name="Time End")]
        [DataType(DataType.Date)]
        public DateTime? TimeEnd { get; set; }

        public string BorrowerNotes { get; set; }
        public string BorrowerRatingId { get; set; }
    }

}
