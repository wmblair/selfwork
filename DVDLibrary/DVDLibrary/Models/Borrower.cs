﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVDLibrary.Models
{
    public class Borrower
    {
        public int BorrowerId { get; set; }
        public string BorrowerName { get; set; }
    }
}