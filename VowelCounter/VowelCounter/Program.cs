﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace VowelCounter
{
    class Program
    {
        private static int pCount;
        private static int pSpaceCount;

        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string to see how many vowels I can find: ");
            string input = Console.ReadLine();
            CountVowels(input);

            Console.Write("\nVowel Count: " + pCount);
            Console.Write("\nSpace Count: " + pSpaceCount);
            Console.Write("\nConsonant Count: {0}", input.Length - (pCount + pSpaceCount));
            Console.ReadLine();
        }

        static void CountVowels(string str)
        {
            int count = 0;
            int spaceCount = 0;
            for (int i = 0; i < str.Length; i++)
            {
                switch (str[i])
                {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':                    
                        count++;
                    break;
                    case ' ':
                        spaceCount++;
                    break;
                }
                    
            }
            pSpaceCount = spaceCount;
            pCount = count;



            //   return count;
        }
    }
}
