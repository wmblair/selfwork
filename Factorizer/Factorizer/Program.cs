﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter a number you would like to factor: ");
            string num = Console.ReadLine();
            int numint = int.Parse(num);
            int counter = 0;
            int sum = 0;

            Console.WriteLine("\nFactors of {0} are:\n", numint);

            for (int i = 1; i < numint; i++)
            {
                if (numint % i == 0)
                {
                    Console.WriteLine(i);
                    Console.WriteLine();
                    counter++;
                    sum += i;
                }
            }

            if (counter == 1)
                Console.WriteLine("{0} is prime!\n", numint);            

            else if (sum == numint)
            {
                Console.WriteLine("{0} is perfect!\n", numint);
                Console.WriteLine("{0} is not a prime", numint);
            }
            else
                Console.WriteLine("{0} is not a prime", numint);

            Console.WriteLine("\nTotal number of factors for this number: {0}", counter);
            Console.ReadLine();
        }
    }
}
