﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StringCalculatorKata;

namespace StringCalculatorKataTests
{
    [TestFixture]
    public class CalculatorTests
    {
        private Calculator _stringCalc;

        [SetUp]
        public void BeforeEachTest()
        {
            _stringCalc = new Calculator();
        }

        [Test]
        public void Add_EmptyStringReturnsZero()
        {
            //Arrange - get a reference to the class we want/instantiate/get object into memory for manipulation
            Calculator calc = new Calculator(); //add new reference under references, and then add using statement
            //Act 
            int result = calc.Add("");

            //Assert - 
            Assert.AreEqual(0,result);

            //can't run a class library
        }

        [Test]
        public void Add_ReturnsNumberWithOneInput()
        {
            //Act
            int result = _stringCalc.Add("5");

            //Assert
            Assert.AreEqual(5,result);
        }

        //[TestCase("1,2", 4)] would fail
        [TestCase("1,2", 3)]
        [TestCase("2,3", 5)]
        [TestCase("10,20", 30)]     //first parameter is input, second is output (what is expected)
        public void Add_HandleTwoNumbers(string input, int expected)
        {
            int result = _stringCalc.Add(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("1,2,3", 6)]
        [TestCase("10,20,30,40",100)]
        [TestCase("1,1,1,1,1", 5)]
        public void Add_HandleUnknownNumberOfArgs(string input, int expected)
        {

            int result = _stringCalc.Add(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("1,2,3", 6)]
        [TestCase("4\n5, 6", 15)]
        [TestCase("10\n20,30\n40", 100)]
        public void Add_HandleNewLinesBetweenNumbers(string input, int expected)
        {
            int result = _stringCalc.Add(input);
            Assert.AreEqual(expected, result);
        }
    }
}
