USE northwind
GO

--1. Write a query showing the customer and order information for customer AROUT.
select * from customers
inner join orders
on customers.CustomerID = orders.CustomerID
where customers.customerid = 'AROUT'

--2. Write a query that combines Orders, Order Details, and Products. Show the OrderID, OrderDate, Order UnitPrice, Product Unit Price, Quantity, Discount, and ProductName.
select orders.orderid, orders.orderDate, [order details].unitprice, products.unitprice, 
products.UnitsInStock, [order details].discount, products.productname from orders
inner join [order details]
on orders.OrderID = [order details].orderID
inner join products
on [order details].productid = products.productid