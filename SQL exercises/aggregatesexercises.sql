use swccorp
GO

--1. Find the Max, Min, and Average Grant amount by Employee ID

select empid, max(Amount) from [grant]
group by empid

select empid, avg(Amount) from [grant]
group by empid

select empid, min(Amount) from [grant]
group by empid
