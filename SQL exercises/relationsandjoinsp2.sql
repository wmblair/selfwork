use northwind
GO

--1. Get a list of each employee and their territories.
select employees.employeeid, employees.firstName, employees.lastName, territories.* from employees 
inner join employeeterritories
on employees.EmployeeID = employeeterritories.EmployeeID
inner join territories
on employeeterritories.TerritoryID = territories.TerritoryID

--2. Get the Customer Name, Order Date, and each order detail�s Product name for USA customers 
select customers.CompanyName, orders.orderdate, products.productname from customers
inner join orders
on customers.customerid = orders.customerid
inner join [order details]
on orders.orderid = [order details].orderid
inner join products
on [order details].ProductID = products.ProductID
where customers.country = 'usa'

--3. Get all the order information where Chai was sold

select products.productname, customers.country, orders.*, [order details].* from customers
inner join orders
on customers.customerid = orders.customerid
inner join [order details]
on orders.orderid = [order details].orderid
inner join products
on [order details].productid = products.productid
where products.productid = 1


use SWCCorp
GO

--1) Write a query to show every combination of employee and location.
select * from employee
cross join location

--2) Find a list of all the Employees who have never found a Grant.
select * from employee e
left outer join [grant] g
on e.empID = g.empid
where g.empid is null