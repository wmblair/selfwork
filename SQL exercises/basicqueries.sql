USE NORTHWIND
GO

--1) Select all of the fields and rows.
select * from products

--2) Find all the product information for the product with the name Queso Cabrales.
select * from products
where ProductName = 'Queso Cabrales'

--3) Using one query, display the name and number of units in stock for the products Laughing Lumberjack Lager, Outback Lager, and Ravioli Angelo.
select ProductName, UnitsInStock from products
where ProductName in ('laughing lumberjack lager', 'outback lager', 'ravioli angelo')

--part 2: 
--1) Select all the order information for the customer QUEDE.
select * from orders
where customerID = 'QUEDE'

--2) Select the orders whose freight is more than $100.00.
select * from orders
where freight > 100

--3) Select the orders shipping to the USA whose freight is between $10 and $20
select * from orders
where shipcountry = 'usa' and freight between 10 and 20