use Northwind
GO

--Expressions
/*
1. In Northwind, print a list of products, the value of the stock (unit price * quantity) 
and sort it by the value from highest to lowest.*/
select products.*, (UnitPrice * unitsinstock) as valueOfStock
from products
order by valueOfStock desc

/*
2. In Northwind, get a list of employees with a column called NameLastFirst which is formatted 
as LastName, FirstName. Sort it alpha by last name, then first name.*/
select (firstname + ' ' + lastname) NameLastFirst, employees.* from employees
order by lastname, firstname

/*
3. Take your query from #1 and create columns to value the stock in Canadian dollars, 
Japanese yen, euros, and pesos given today�s exchange rates.*/
select products.*, (UnitPrice * unitsinstock) as USvalueOfStock, ((UnitPrice * unitsinstock)*0.77) as CAvalueOfStock,
((UnitPrice * unitsinstock)*0.0094) as JPvalueOfStock, ((UnitPrice * unitsinstock)*1.13) as EUvalueOfStock,
((UnitPrice * unitsinstock)*0.054) as MXvalueOfStock from products
--order by valueOfStock desc