/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [CustomerID]
      ,[CustomerType]
      ,[FirstName]
      ,[LastName]
      ,[CompanyName]
  FROM [MovieCatalog].[dbo].[Customer]

use SWCCorp
GO

--1. Sally (employeeID 11) is getting married.  Change her last name to Green.
update employee
set lastname = 'Green'
where empid = 11

/*
2. All the employees in the Spokane location are becoming contractors. 
Update their status field to External.*/
update employee
set [status] = 'External'
where employee.locationid = 4

--3. The location for Seattle has a typo. Update the street field to read 111 1st Ave.
update location
set street = '111 1st Ave.'
where locationid = 1

--4. A new policy requires that grants for employees in Boston be made for $20,000. There are two Boston records which aren’t set to $20,000.  Please fix them!
update [grant]
set amount = 20000
where empid in (3,10)

--1. We are moving our G movies to a kids website. Delete all the G rated movies.
use MovieCatalog
GO

delete from Movies
where Rating = 'G'

/*
2. SWCCorp is not teaching long classes for management any more. 
Delete all records where the duration is more than 20 hours.*/

use SWCCorp
GO

delete from MgmtTraining
where ClassDurationHours > 20

