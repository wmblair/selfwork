use SWCCORP
GO

--1. Show all the records from the Grant Table, sorted alphabetically by Grant Name.
select * from [grant]
order by grantname

--2. Show all the employees in the Employee table from newest hire to oldest.
select * from employee
order by HireDate desc

--3. Query the Current Products table for just the ProductName and Category fields, ordered from most expensive to least expensive retail price.
use Northwind
GO

select products.productName, categories.*
from products
inner join categories
on products.categoryid = categories.categoryid
order by products.UnitPrice desc

--4. Sort the grant table from highest to lowest amount. If there is a tie, sort the ties alphabetically by grant name.
use SWCCorp
GO

select * from [grant]
order by [grant].Amount desc, grantName

/*5. Join the Employee and Location tables using an Outer Join that shows all employee records 
even if they have no location; show the FirstName, LastName, and City such that NULL cities 
show up first.
*/

select firstname, lastname, city from employee
left join location
on employee.LocationID = location.LocationID
order by city
