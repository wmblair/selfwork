/*
1. Write a parameterized query to select the
grant information from SWCCorp�s Grant
table to show grants between a minimum
and maximum value.
*/

use SWCCorp
GO

declare @minAmount int = 20000
declare @maxAmount int = 25000

select * from [grant]
where [grant].Amount between @minAmount and @maxAmount


/*
1. SWCCorp needs a stored procedure called GetProductListByCategory 
that should take in the category name as a parameter.


create procedure GetProductListByCategory
(
	@categoryName varchar(30)	
) as
select * from currentproducts
where currentproducts.Category = @categoryname
*/

declare @categoryName varchar(30)
set @categoryName = 'No-Stay'

exec GetProductListByCategory @categoryName

/*
2. SWCCorp needs a stored procedure called
GetGrantsByEmployee that takes in the
LastName as a parameter and returns the
EmployeeId, FirstName, LastName,
GrantName, and Amount for that employee�s
grants.

create procedure GetGrantsByEmployee
(
	@lastName varchar(30)	
) as

select [grant].EmpID, employee.FirstName, employee.LastName, [grant].GrantName, [grant].Amount from [grant]
inner join employee
on employee.empId = [grant].empId
where employee.LastName = @lastName
*/

declare @lastName varchar(30)
set @lastName = 'Lonning'

exec GetGrantsByEmployee @lastName

/*
Write an update and an insert procedure for the
SWCCorp Grant table that takes a grantID as a
parameter.

create procedure UpdateGrantById
(
	@grantid int	
) as

update [grant]
set [grant].Amount = 0
where [grant].GrantId = @grantid
*/

declare @grantid int
set @grantid = 004

exec UpdateGrantById @grantid
