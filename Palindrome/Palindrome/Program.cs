﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string so I can determine if it is a palindrome: ");
            string s = Console.ReadLine();
            s = s.Trim();

            if(IsPalindrome(s))
            Console.WriteLine("Your string is a palindrome!");
            else
            Console.WriteLine("Your string isn't a palindrome. Too bad :(");

            Console.ReadLine();
        }

        static bool IsPalindrome(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == ' ')
                    str = str.Remove(i, 1);
            }

            for (int i = 0; i < str.Length/2; i++)
            {
                if (str[i] != str[str.Length - 1 - i])
                    return false;
            }
            return true;
        }
    }
}
