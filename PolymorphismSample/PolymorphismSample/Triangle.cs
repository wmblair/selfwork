﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolymorphismSample
{
    class Triangle : Shape
    {
        public override string Draw()
        {
            return "Drawing a triangle (3 sides)";
        }
    }
}
