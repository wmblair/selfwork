﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
//using AdoDotNetExampleWebSiteThing2.Repos;
//using AdoDotNetExampleWebSiteThing2.ViewModels;

namespace AdoDotNetExampleWebSiteThing2.Controllers
{
    public class EmployeeController : Controller
    {
        /*
        private EmployeeRepository employeeRepository;
        private LocationRepository locationRepository;

        public EmployeeController()
        {
            employeeRepository = new EmployeeRepository();
            locationRepository = new LocationRepository();
        }

        // GET: Employee
        public ActionResult DisplayAllEmployees()
        {
            var employees = employeeRepository.GetAllEmployees();

            return View(employees);
        }

        public ActionResult Details(int id)
        {
            var viewModel = new EmployeeDetailsViewModel();
            var employee = employeeRepository.GetEmployee(id);

            if (employee.LocationId != null)
            {
                var location = locationRepository.GetLocation((int) employee.LocationId);
                viewModel.Location = location;
            }

            viewModel.Employee = employee;

            return View(viewModel);
        }

        public ActionResult AddEmployee()
        {
            var viewModel = new EmployeeDetailsViewModel();

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddEmployee(EmployeeDetailsViewModel viewModel)
        {
            if (viewModel.Employee.EmpId == 0)
                employeeRepository.AddEmployee(viewModel.Employee);
            else
            {
                var employeeInDb = employeeRepository.GetEmployee(viewModel.Employee.EmpId);
                employeeInDb.FirstName = viewModel.Employee.FirstName;
                employeeInDb.LastName = viewModel.Employee.LastName;
                employeeInDb.HireDate = viewModel.Employee.HireDate;
                employeeInDb.Status = viewModel.Employee.Status;
                employeeRepository.UpdateEmployee(employeeInDb);
            }

            return RedirectToAction("DisplayAllEmployees");
        }

        public ActionResult DeleteEmployee(int id)
        {
            var employee = employeeRepository.GetEmployee(id);

            if (employee == null)
                return HttpNotFound();
            else
                employeeRepository.DeleteEmployee(employee);

            return RedirectToAction("DisplayAllEmployees");
        }

        public ActionResult EditEmployee(int id)
        {
            var employee = employeeRepository.GetEmployee(id);

            if (employee == null)
                return HttpNotFound();

            var viewModel = new EmployeeDetailsViewModel();
            viewModel.Employee = employee;

            return View("AddEmployee", viewModel);
        }
        */
    }
}