﻿﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
//using AdoDotNetExampleWebSiteThing.Config;
//using AdoDotNetExampleWebSiteThing.Models;
//using AdoDotNetExampleWebSiteThing.Utility;

namespace AdoDotNetExampleWebSiteThing2.Repos
{
    public class EmployeeRepository
    {
        /*
        public List<Employee> GetAllEmployees()
        {
            var employees = new List<Employee>();

            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = @"select * from Employee";

                cmd.Connection = cn;
                cn.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        employees.Add(Mapper.PopulateFromDataReader(dr));
                    }
                }
            }

            return employees;
        }

        public Employee GetEmployee(int id)
        {
            var employee = new Employee();

            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText =
                    @"select * from Employee as e where e.EmpID = @EmployeeID";

                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@EmployeeID", id);

                cn.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        employee = Mapper.PopulateFromDataReader(dr);
                    }
                }
            }

            return employee;
        }

        public void AddEmployee(Employee employee)
        {
            var query =
                "insert into dbo.Employee (EmpID, FirstName, LastName, HireDate, [Status]) values (@EmpID, @FirstName, @LastName, @HireDate, @Status)";

            using (var cn = new SqlConnection(Settings.ConnectionString))
            using (var cmd = new SqlCommand(query, cn))
            {
                cmd.Parameters.AddWithValue("@EmpID", 100); // hardcoded just to get stupid thing working.
                cmd.Parameters.AddWithValue("@FirstName", employee.FirstName);
                cmd.Parameters.AddWithValue("@LastName", employee.LastName);
                cmd.Parameters.AddWithValue("@HireDate", employee.HireDate);
                cmd.Parameters.AddWithValue("@Status", employee.Status);

                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        public void DeleteEmployee(Employee employee)
        {
            var query = "delete from Employee where Employee.EmpID = @EmpID";

            using (var cn = new SqlConnection(Settings.ConnectionString))
            using (var cmd = new SqlCommand(query, cn))
            {
                cmd.Parameters.AddWithValue("@EmpID", employee.EmpId);

                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        public void UpdateEmployee(Employee employeeInDb)
        {
            var query =
                "update Employee set FirstName = @FirstName, LastName = @LastName, HireDate = @HireDate, [Status] = @Status where EmpID = @EmpID";

            using (var cn = new SqlConnection(Settings.ConnectionString))
            using (var cmd = new SqlCommand(query, cn))
            {
                cmd.Parameters.AddWithValue("@EmpID", employeeInDb.EmpId);
                cmd.Parameters.AddWithValue("@FirstName", employeeInDb.FirstName);
                cmd.Parameters.AddWithValue("@LastName", employeeInDb.LastName);

                if (employeeInDb.HireDate == null)
                    cmd.Parameters.AddWithValue("@HireDate", DBNull.Value);
                else

                    cmd.Parameters.AddWithValue("@HireDate", employeeInDb.HireDate.Value);
                if (employeeInDb.Status == null)
                    cmd.Parameters.AddWithValue("@Status", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@Status", employeeInDb.Status);

                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        */
    }
}