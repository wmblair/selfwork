﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BaseballLeagueWebAPI.Models;

namespace BaseballLeagueWebAPI.Repos
{
    public class FakeTeamRepository
    {
        private static List<Team> _teamList;

        static FakeTeamRepository()
        {
            _teamList = new List<Team>
            {
                new Team {
                    TeamId = 1,
                    TeamName = "Cleveland Indians",
                    Manager = "Terry Francona",
                    League = "American League"
                },
                new Team {
                    TeamId = 2,
                    TeamName = "Cincinnati Reds",
                    Manager = "Bryan Price",
                    League = "National League"
                },
                new Team {
                    TeamId = 3,
                    TeamName = "Chicago White Sox",
                    Manager = "Robin Ventura",
                    League = "American League"
                }

            };
        }

        public List<Team> GetAllTeams()
        {
            return _teamList;
        }

        public void Add(Team team)
        {
            if (_teamList.Any())
                team.TeamId = _teamList.Max(t => t.TeamId) + 1;
            else
                team.TeamId = 1;

            _teamList.Add(team);
        }

        public void Delete(int id)
        {
            _teamList.RemoveAll(t => t.TeamId == id);
        }

        public void Edit(Team team)
        {
            Delete(team.TeamId);
            _teamList.Add(team);
        }

        public Team GetById(int id)
        {
            return _teamList.FirstOrDefault(t => t.TeamId == id);
        }
        
    }
}