﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseballLeagueWebAPI.Models
{
    public class Team
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public string Manager { get; set; }
        public string League { get; set; }
    }
}