﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BaseballLeagueWebAPI.Models;
using BaseballLeagueWebAPI.Repos;

namespace BaseballLeagueWebAPI.Controllers
{
    public class TeamsController : ApiController
    {
        public List<Team> Get()
        {
            var repo = new FakeTeamRepository();
            return repo.GetAllTeams();
        }

        public HttpResponseMessage Post(Team newTeam)
        {
            var repo = new FakeTeamRepository();
            repo.Add(newTeam);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            string uri = Url.Link("DefaultApi", new {id = newTeam.TeamId}); //mapping the location of the new object using 'api/controller' path
            response.Headers.Location = new Uri(uri); //setting the location within the header of the response equal to the object's path represented as a string
                                                        //...so that upon reloading the list, everything in each object will be set
            return response;
        }
    }
}
