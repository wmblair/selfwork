﻿/// <reference path="../tinymce/tinymce.js" />
/// <reference path="../tinymce/tinymce.min.js" />

$(document)
    .ready(function () {
        $('#btnShowAddTeam')
            .click(function () {

                $('#addTeamModal').modal('show');


                $('#addTeamModal').bind('shown', function () {
                    tinyMCE.execCommand('mceAddControl', false);
                });
                tinymce.init({
                    selector: '#name',
                    height: 500,
                    plugins: [
                      'advlist autolink lists link image charmap print preview anchor',
                      'searchreplace visualblocks code fullscreen',
                      'insertdatetime media table contextmenu paste code'
                    ],
                    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    content_css: [
                      '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                      '//www.tinymce.com/css/codepen.min.css'
                    ]
                });


                $('#name').val('');
                $('#manager').val('');
                $('#league').val('');
            });

        $('#btnSaveTeam')
            .click(function () {
                var team = {};

                team.TeamName = tinyMCE.activeEditor.getContent({ format: 'text' }); 
                //extra field in model to send plain text to database?

                //team.TeamName = tinymce.get('name').getContent();

                //team.Name = $('#name').val();
                team.Manager = $('#manager').val();
                team.League = $('#league').val();

                $.post(teamPath, team) //makes AJAX request to create new object in the teampath
                    .done(function () {

                        loadTeams();
                        $('#addTeamModal').modal('hide');

                        $('#addTeamModal').bind('hide', function () {
                            tinyMCE.execCommand('mceRemoveControl', false);
                        });

                    })
                    .fail(function (jqXhr, status, err) {
                        alert(status + ' - ' + err);
                    });
            });
    });