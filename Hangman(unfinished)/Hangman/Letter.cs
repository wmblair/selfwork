﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    public class Letter
    {
        public char Value { get; set; }
        public bool IsGuessed { get; set; }
    }

}
