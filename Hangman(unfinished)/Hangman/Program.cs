﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    class Program
    {
        static void Main(string[] args)
        {
            PlayGame();
            Console.ReadLine();
        }

        static void PlayGame()
        {
            Console.WriteLine("Hangman!");
            
            //Console.WriteLine("Enter a word that you will guess: ");
            //Make array of used words

            string word = Console.ReadLine();
                
                //= "this is a test";

            List<Letter> wordToGuess = new List<Letter>();
            wordToGuess = WordIntoArray(word.Trim(), wordToGuess);

            do
            {
                Console.Clear();

                PrintWord(wordToGuess);

                Console.WriteLine("\nEnter a character for the word: ");
                string input = Console.ReadLine();
                char guess = ' ';

                if (input != " " && input.Length > 0)
                    guess = input[0];                
                else
                    continue;
                wordToGuess = CheckGuess(wordToGuess, guess);

            } while (!IsSolved(wordToGuess));

            Console.Clear();
            PrintWord(wordToGuess);
            Console.WriteLine("\nCongratulations! You Won!");
        }

        static List<Letter> CheckGuess(List<Letter> letterList, char guess)
        {
            int count = 0;
            for (int i = 0; i < letterList.Count; i++)
            {
                if (letterList[i].Value == guess)
                    letterList[i].IsGuessed = true;                
            }

            return letterList;
        }

        static void PrintWord(List<Letter> letterList)
        {
            foreach (var letter in letterList)
            {
                if (letter.Value != ' ' && !letter.IsGuessed)
                    Console.Write("_ ");       
                else if(letter.Value == ' ')
                    Console.Write(" ");         
                else
                    Console.Write(letter.Value + " ");
            }
        }

        static List<Letter> WordIntoArray(string word, List<Letter> letterList)
        {
            for (int i = 0; i < word.Length; i++)
            {
                if(word[i] == ' ')
                    letterList.Add(new Letter() {Value = ' ', IsGuessed = true});
                else
                    letterList.Add(new Letter() {Value = word[i], IsGuessed = false});
            }
            return letterList;
        }

        static bool IsSolved(List<Letter> letterList)
        {
            for (int i = 0; i < letterList.Count; i++)
            {
                if (!letterList[i].IsGuessed)
                    return false;
            }
            return true;
        }
    }
}
