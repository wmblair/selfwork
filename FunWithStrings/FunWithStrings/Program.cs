﻿using System;
using System.Collections.Generic;
<<<<<<< HEAD
using System.Globalization;
=======
>>>>>>> 7bff06825b6dfbed2e24594574aac237dfa6335b
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
<<<<<<< HEAD
using Microsoft.SqlServer.Server;
=======
>>>>>>> 7bff06825b6dfbed2e24594574aac237dfa6335b

namespace FunWithStrings
{
    class Program
    {
        static void Main(string[] args)
        {
<<<<<<< HEAD
            Console.WriteLine("Fun with strings");
=======
            Console.WriteLine("******** Fun With Strings ********");
>>>>>>> 7bff06825b6dfbed2e24594574aac237dfa6335b
            //BasicStringFunctionality();
            //StringConcatenation();
            //EscapeCharacters();
            //StringEquality();
            //StringsAreImmutable();
            StringBuilderExample();
        }

<<<<<<< HEAD
        #region basicstringfunctionality

        static void BasicStringFunctionality()
        {
            Console.WriteLine("=> Basic String Functionality");
            string firstName = "Freddy";
            Console.WriteLine($"Value of firstName: {firstName}");
                //string interpolation, alternative way of using string placeholders
            Console.WriteLine($"firstName has {firstName.Length} characters.");
            Console.WriteLine("firstName in uppercase: {0}", firstName.ToUpper());
            Console.WriteLine("firstName in lowercase: {0}", firstName.ToLower());
            Console.WriteLine("firstName contains the letter y? {0}", firstName.Contains("y"));
            Console.WriteLine("firstName after replace: {0}", firstName.Replace("dy", ""));
            Console.ReadLine();
            Console.Clear();
        }

        #endregion

        #region stringconcatenation

        static void StringConcatenation()
        {
            Console.WriteLine("=> String concatenation");
            string s1 = "This is the first ";
            string s2 = "part of the string";
            string s3 = String.Concat(s1, s2);
            string s4 = s1 + s2;
            //'String' is for the .NET framework, can be used when creating and compiling languages using .NET
            //'string' is specifically for C#

            Console.WriteLine(s3);
            Console.WriteLine(s4);
            Console.ReadLine();
            Console.Clear();
        }

        #endregion

        #region escapecharacters

        static void EscapeCharacters()
        {
            Console.WriteLine("=> Escape characters:\a");
            string strWithTabs = "Model\tColor\tSpeed\tPet Name\a";
            Console.WriteLine(strWithTabs);
            Console.ReadLine();

            Console.WriteLine("Everyone loves \"Hello World\"");
            Console.WriteLine("C:\\MyApp\\bin\\Debug\\somefile.txt");
            Console.ReadLine();

            //verbatim strings
            Console.WriteLine(@"C:\MyApp\bin\Debug\somefile.txt");
            Console.WriteLine(@"Will wrote the ""Hello World"" app");
            Console.ReadLine();

            Console.ReadLine();
        }

        #endregion

        #region stringequality

=======
        static void StringBuilderExample()
        {
            Console.WriteLine("String Builder Example");

            StringBuilder sb = new StringBuilder();

            sb.Append("List of movies");
            sb.Append("\nShawshank Redemption");
            sb.Append("\nGroundhog day");
            sb.Append("\nIron Man");
            sb.Append("\nStar Wars");
            Console.WriteLine(sb);
            Console.ReadLine();
        }

        static void StringsAreImmutable()
        {
            string s1 = "This is my string";
            Console.WriteLine("s1 = {0}", s1);

            string upperString = s1.ToUpper();
            Console.WriteLine("upperString = {0}", upperString);

            Console.WriteLine("s1 = {0}", s1);
            Console.ReadLine();
        }

>>>>>>> 7bff06825b6dfbed2e24594574aac237dfa6335b
        static void StringEquality()
        {
            Console.WriteLine("=> String Equality");
            string s1 = "Hello!";
            string s2 = "Yo";
            string s3 = "Yo";
            Console.WriteLine("s1 = {0}", s1);
            Console.WriteLine("s2 = {0}", s2);
            Console.WriteLine();

<<<<<<< HEAD
            Console.WriteLine("s1 == s2: {0}", s1 == s2);

=======
            // Test these strings for equality
            Console.WriteLine("s1 == s2: {0}", s1 == s2);
>>>>>>> 7bff06825b6dfbed2e24594574aac237dfa6335b
            Console.WriteLine("s1 == Hello!: {0}", s1 == "Hello!");
            Console.WriteLine("s1 == HELLO!: {0}", s1 == "HELLO!");
            Console.WriteLine("s1.Equals(s2): {0}", s1.Equals(s2));
            Console.WriteLine("s2.Equals(s3): {0}", s2.Equals(s3));
<<<<<<< HEAD

            Console.ReadLine();
        }

        #endregion

        #region stringsareimmutable
        static void StringsAreImmutable()
        {
            string s1 = "This is my string";
            Console.WriteLine("s1 = {0}", s1);

            string upperString = s1.ToUpper();
            Console.WriteLine("upperString = {0}", upperString);

            Console.WriteLine("s1 = {0}", s1); //original value of s1 doesn't change
            Console.ReadLine();

            string myString = "a";
            myString = myString + "b"; //memory allocated for 'a' gets destroyed, recreates 'ab'
            //creates a lot of overhead when adding a single character to a long string

            //stringbuilder has better performance than the above process
        }
        #endregion

        #region stringbuilderexample
        static void StringBuilderExample()
        {
            Console.WriteLine("String Builder Example");
            StringBuilder sb = new StringBuilder();

            sb.Append("List of movies:");
            sb.Append("\nShawshank Redemption");
            sb.Append("\nGroundhog Day");
            sb.Append("\nIron Man");
            sb.Append("\nStar Wars");
            //Console.WriteLine(sb.ToString()); //converting from object to string
            Console.WriteLine(sb); //uses string by default
        }
        #endregion

        //encapsulation: making code a black box so that the user doesn't actually have to know a lot about what they're calling
        //the user just expects an output, they shouldn't know a lot about the method to get that output

        //loosely coupled code still needs certain pieces from your code, but doesn't care where it gets it
        //make the method pluggable

        //test driven development: write a test first, then write the program so that you plan beforehand

=======
            Console.ReadLine();
        }

        #region Basic String Functionality
        static void BasicStringFunctionality()
        {
            Console.WriteLine("=> Basic String Functionality");
            string firstName = "Freddy";
            Console.WriteLine($"Value of firstName: {firstName}");
            Console.WriteLine($"firstName has {firstName.Length} characters.");
            Console.WriteLine("firstName in uppercase: {0}", firstName.ToUpper());
            Console.WriteLine("firstName in lowercase: {0}", firstName.ToLower());
            Console.WriteLine("firstName contains the letter y? {0}",
                firstName.Contains("y"));
            Console.WriteLine("firstName after replace: {0}", 
                firstName.Replace("dy", ""));
            Console.ReadLine();
            Console.Clear();
        }
        #endregion

        #region String Concatenation

        static void StringConcatenation()
        {
            Console.WriteLine("=> String Concatenation");
            string s1 = "This is the first ";
            string s2 = "part of the string";
            string s3 = string.Concat(s1, s2);
            string s4 = s1 + s2;
            Console.WriteLine(s3);
            Console.WriteLine(s4);
            Console.ReadLine();
            Console.Clear();
        }

        #endregion

        static void EscapeCharacters()
        {
            Console.WriteLine("=> Escape characters:\a");
            string strWithTabs = "Model\tColor\tSpeed\tPet Name\a";
            Console.WriteLine(strWithTabs);
            Console.ReadLine();
            
            Console.WriteLine("Everyone loves \"Hello World\"");
            Console.WriteLine("C:\\MyApp\\bin\\Debug\\somefile.txt");

            Console.WriteLine(@"C:\MyApp\bin\Debug\somefile.txt");

            Console.WriteLine(@"Dave wrote the ""Hello World""");

            Console.ReadLine();
        }
>>>>>>> 7bff06825b6dfbed2e24594574aac237dfa6335b
    }
}
