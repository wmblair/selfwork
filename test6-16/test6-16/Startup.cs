﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(test6_16.Startup))]
namespace test6_16
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
