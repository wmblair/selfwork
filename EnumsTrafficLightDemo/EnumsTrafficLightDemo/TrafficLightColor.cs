﻿namespace EnumsTrafficLightDemo
{
    public enum TrafficLightColor //can be used in other classes, not meant to be used within a particular class                                   //...unless you want only that class to have access to it
    {
        Green, //not actually strings, special types of labels used to associate text/descriptions with numeric values
        Yellow,
        Red
    }
}
