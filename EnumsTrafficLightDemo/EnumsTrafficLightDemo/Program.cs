﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumsTrafficLightDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //int value = (int)test.testcolors.Yellow;
            //enums don't need an instance of a class to be accessed (test class doesn't need an instance for its enum to be accessed),
            //so there isn't really any reason to have them in any class in the first place

            //instantiate a traffic light class
            //print value of the color
            //change the light color multiple times and print each time

            TrafficLight light = new TrafficLight();
            PrintValue(light);

            light.ChangeColor();
            PrintValue(light);

            light.ChangeColor();
            PrintValue(light);

            light.ChangeColor();
            PrintValue(light);

            Console.ReadLine();

        }

        static void PrintValue(TrafficLight light)
        {
            Console.WriteLine("The light has a value of {0} which is the color {1}",
                (int)light.CurrentColor, Enum.GetName(typeof(TrafficLightColor), (int)light.CurrentColor));
        }
    }
}
