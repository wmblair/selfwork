﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HylandApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myArray = {1, 5, 3, 7, 6, 4, 9, 8, 0, 2};

            string str1 = "AbCCdefgfhIJBlm";
            string str2 = "Aipzfdhshsihdg";

           // PrintDuplicates(myArray); //5 7 4

           // myArray = SelectionSort(myArray);
           // PrintSort(myArray);
           PrintDuplicates(myArray);
            //            PrintPrimes();
            Console.ReadLine();


        }

        static void PrintSort(int[] array)
        {
            foreach (int i in array)
            {
                Console.Write(i + " ");
            }
        }

        static int[] SelectionSort(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                int smallest = i;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[j] < array[smallest])
                        smallest = j;
                }
                int temp = array[i];
                array[i] = array[smallest];
                array[smallest] = temp;
            }
            return array;
        }

        static void OutputNumbers()
        {
            for (int i = 1; i <= 100; i++)
            {
                if (i%3 != 0 && i%7 != 0)
                    Console.WriteLine(i);
                else
                {

                    if (i%3 == 0)
                        Console.Write("On");
                    if (i%7 == 0)
                        Console.Write("Base");

                    Console.WriteLine();
                }
            }
        }

        static bool IsAnagram(string str1, string str2)
        {
            if (str1.Length != str2.Length)
                return false;

            int[] letterIndices = new int[58];

            for (int i = 0; i < str1.Length; i++)
            {
                letterIndices[str1[i] - 65]++;
                letterIndices[str2[i] - 65]++;
            }

            for (int i = 0; i < letterIndices.Length; i++)
            {
                if (letterIndices[i]%2 != 0)
                    return false;
            }
            return true;
        }

        static void MostFrequentLetters(string str)
        {
            int[] alphaCount = new int[str.Length];

            for (int i = 0; i < str.Length; i++)
            {
                alphaCount[i] = 1;
                for (int j = i + 1; j < str.Length; j++)
                {
                    if (str[j].ToString().ToUpper() == str[i].ToString().ToUpper())
                        alphaCount[i]++;
                }
            }
            int highest = 0;

            for (int i = 0; i < str.Length; i++) //get highest count in a particular index
                if (highest < alphaCount[i])
                    highest = alphaCount[i];

            for (int i = 0; i < str.Length; i++)
            {
                if (alphaCount[i] != 1 && alphaCount[i] == highest)
                    Console.Write(str[i]);
            }
        }

        static void PrintDuplicates(int[] array)
        {
            int[] intCount = new int[array.Length];
            ArrayList myList = new ArrayList();

            for (int i = 0; i < array.Length; i++)
                myList.Add(array[i]);

            for (int i = 0; i < array.Length; i++)
            {
                intCount[i] = 1;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] == array[j])
                    {
                        intCount[i]++;
                        myList[j] = 0;
                    }
                }
            }

            for (int i = 0; i < myList.Count; i++)
            {
                if (intCount[i] != 1 && (int)myList[i] != 0)
                    Console.Write(myList[i]);
            }
        }

        static void PrintPrimes()
        {
            Console.WriteLine(2);
            for (int i = 3; i < 100; i+=2)
            {
                for (int j = 1; j < i; j+=2)
                {
                    if (i%j == 0 && j != 1)
                        break;
                    if (j == i-2)
                        Console.WriteLine(i);
                }                
            }
        }
    }
}
