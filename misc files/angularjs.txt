angular js notes:
scope is the equivalent of exposing a public property (making variables accesible from other areas)

data-ng-controller: 
-is set equal to a view model that you can bind to
-anything declared within this specification can be bound to the view model
-when this is initialized, anything from that point down is bounded to the view, and nothing outside of it will have access to the viewmodel
-If initializer is in body, then anything inside of body will have access to the viewmodel

-angular refreshes the view automatically once the viewmodel is changed
-knockout.js refreshes just the viewmodel bound to a specific event, and angular refreshes the entire page based on if anything in any viewmodel changes

MVC routing engine vs Angular routing engine (doesn't make any server trips)

Bootstrapping angular: initializing, or starting, your angular app. automatic: ng-app

A portion of the DOM can be treated as an angular application

Normally when a html page loads, the web browser parses HTML into the DOM automatically.
but when an angular application is specified in the html document (either with ng-app or with angular.bootstrap), that portion of the html is compiled by angularjs

an angular app can have multiple controllers that can be referenced in multiple HTML documents
The angular controller (which can loosely be considered a function) can be called anything you want, but any http requests made in it must reference an existing API Controller

all http requests made in js file using angular are ajax requests/calls (XMLHttpRequest)

XMLHttpRequest is an API that provides client functionality for transferring data between a client and a server. It provides an easy way to retrieve data from a URL without having to do a full page refresh.

ASP.NET Web API is an ideal platform for building RESTful applications on the .NET framework

ASP.NET Web API is a framework for building HTTP services that can be consumed by a broad range of clients including browsers, mobiles, iphone and tablets.

An API is a set of routine definitions, protocols, and tools for building software and applications.

Web APIs are the defined interfaces through which interactions happen between an enterprise and applications that use its assets. An API approach is an architectural approach that revolves around providing programmable interfaces to a set of services to different applications serving different types of consumers.[10] When used in the context of web development, an API is typically defined as a set of Hypertext Transfer Protocol (HTTP) request messages, along with a definition of the structure of response messages, which is usually in an Extensible Markup Language (XML) or JavaScript Object Notation (JSON) format.