﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContactList.Data;
using ContactList.Models;

namespace ContactList.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var repo = new FakeContactRepository();
            var model = repo.GetAll();
            return View(model);
        } //selects the view and sends data to the view

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var repo = new FakeContactRepository();
            var contact = repo.GetById(id);
            return View(contact);
        }

        [HttpGet]
        public ActionResult Add()
        {                       //need a view to support this method
            return View(); //will only respond to GET requests
        }

        [HttpPost]
        public ActionResult AddContact(Contact contact)
        {
            var repo = new FakeContactRepository();
            repo.Add(contact);

            return RedirectToAction("Index");
        }

        public ActionResult EditContact(Contact contact)
        {
            var repo = new FakeContactRepository();
            repo.Edit(contact);

            return RedirectToAction("Index");
        }

        public ActionResult RemoveContact(int id)
        {
            var repo = new FakeContactRepository();
            repo.Delete(id);

            return RedirectToAction("Index");
        }

        /*
        [HttpPost]
        public ActionResult AddContact() //what will the user see after they posted their input?
        {
            var contact = new Contact();
            contact.Name = Request.Form["Name"];
            contact.PhoneNumber = Request.Form["Phone"];
            var repo = new FakeContactRepository();
            repo.Add(contact);
            return RedirectToAction("Index"); //goes back to index method to display updated list
        }
        */

    }
}