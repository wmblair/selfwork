﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RollDieCount
{
    class Program
    {
        static void Main(string[] args)
        {
            RandomRolls();
            Console.ReadLine();
        }

        static void RandomRolls()
        {
            int[] countArray = {0, 0, 0, 0, 0, 0};
            Random roll = new Random();

            for (int i = 0; i < 100; i++)
            {
                int rand = roll.Next(1, 7);
                countArray[rand - 1]++;
                Console.WriteLine("Roll " + (i+1) + ": " + rand);
            }

            for (int i = 0; i < countArray.Length; i++)
                Console.WriteLine("Number of times " + (i+1) + " was rolled: " + countArray[i]);
        }
    }
}
