﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Sockets;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    public class Exercises
    {

        public void Ex01()
        {
            //Find all products that are out of stock...
            var products = DataLoader.LoadProducts();
            //var customers = DataLoader.LoadCustomers();
            //var numA = DataLoader.NumbersA;

            var results = products.Where(p => p.UnitsInStock == 0);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        public void Ex02()
        {
            var products = DataLoader.LoadProducts();
            var results = products.Where(p => p.UnitsInStock > 0 && p.UnitPrice > 3);

            foreach (var product in results)
            {
                Console.WriteLine(product.UnitsInStock + ", " + product.UnitPrice);
            }
        }

        public void Ex03()
        {
            var customers = DataLoader.LoadCustomers();
            var results = customers.Where(c => c.Region == "WA")
                .Select(c => c.Orders.Select(c2 => new {c.CustomerID, c2.OrderID}));

            foreach (var customer in results)
            {
                foreach (var a in customer) {
                    
                    Console.WriteLine(a.CustomerID + ", " + a.OrderID);
                }
                Console.WriteLine();
            }

        }

        public void Ex04()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                select p.ProductName;

            foreach (var product in results)
            {
                Console.WriteLine(product);
            }

        }

        public void Ex05()
        {
            var products = DataLoader.LoadProducts();
            var results = products.Select(p => new {OriginalPrice = p.UnitPrice,
                increaseby25Perc = p.UnitPrice + (p.UnitPrice*(decimal)0.25)});

            foreach (var product in results)
            {
                Console.WriteLine(product);
            }
        }

        public void Ex06()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                select p.ProductName.ToUpper();

            foreach (var product in results)
            {
                Console.WriteLine(product);
            }
        }

        public void Ex07()
        {
            var products = DataLoader.LoadProducts();
            var results = products.Where(p => p.UnitsInStock % 2 == 0);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName + ", " + product.UnitsInStock);
            }
        }

        public void Ex08()
        {
            //Console.WriteLine("Not Implemented");
            //8. Create a new sequence of products with ProductName, Category, and rename UnitPrice to Price.
            var products = DataLoader.LoadProducts();
            var results = from p in products
                select new
                {
                    p.ProductName,
                    p.Category,
                    Price = p.UnitPrice
                };

            foreach (var product in results)
            {
              //  Console.WriteLine(product.ProductName + ", " + product.Category + ", " + Price);
              Console.WriteLine(product);
            }

        }

        public void Ex09() 
            //9. Make a query that returns all pairs of numbers from both arrays such that the number from numbersB is less than the number from numbersC.
        {
            var numB = DataLoader.NumbersB;
            var numC = DataLoader.NumbersC;

            var results =
                numB.Select((val, index) => new {valB = val, valC = numC[index]})
                .Where(item => item.valB < item.valC);

            foreach (var r in results)
            {
                Console.WriteLine($"{r.valB} - {r.valC}");
            }
        }

        public void Ex10()
        { 
            var customers = DataLoader.LoadCustomers();
            var results = customers.Select(customer => (customer.Orders.Where(c => c.Total < 500)
                .Select(c => new {customer.CustomerID, c.OrderID, c.Total})));
        //    .OrderBy(c2 => c2.Total) not working?

            foreach (var r in results)
            {
                foreach (var c in r)
                {
                    Console.WriteLine("Customer ID: {0}, Order ID: {1}, Order Total: {2}", 
                        c.CustomerID, c.OrderID, c.Total);
                }
            }
        }

        public void Ex11()
        {
            var numbersA = DataLoader.NumbersA;
            var results = numbersA.Take(3); 

            foreach (var n in results)
            {
                Console.WriteLine(n);
            }

        }

        public void Ex12() // ?
        {
            var customers = DataLoader.LoadCustomers();
            var results = customers.Where(c => c.Region == "WA")
                .Select(c => c.Orders.Select(c2 => new {c.CustomerID, c2.OrderID}).Take(3));

            foreach (var c in results)
            {
                foreach (var a in c)
                {
                    Console.WriteLine(a.CustomerID + ", " + a.OrderID);
                }
            }

        }

        public void Ex13()
        {
            var numbersA = DataLoader.NumbersA;

            var results = (numbersA.Select(c => c)).Skip(3);

                foreach (var r in results)
                {
                    Console.WriteLine(r);
                }
        }

        public void Ex14()
        {
            var customers = DataLoader.LoadCustomers();
            var results = customers.Where(c => c.Region == "WA")
                .Select(c => c.Orders.Select(c2 => new {c.CustomerID, c2.OrderID}).Skip(2));

            foreach (var c in results)
            {
                foreach (var a in c)
                {
                    Console.WriteLine(a.CustomerID + ", " + a.OrderID);
                }
            }
        }

        public void Ex15()
        {
            var numbersC = DataLoader.NumbersC;
            var results = numbersC.Where(n => n <= 5).OrderBy(n => n);
            //does this need to be sorted?

            foreach (var number in results)
            {
                Console.WriteLine(number);
            }
        }

        public void Ex16()
        {
            var numbersC = DataLoader.NumbersC;
            var results = numbersC.Select((value, index) => 
            new {Num = value, Pos = index}).TakeWhile(n => n.Num > n.Pos);
            //above statement returns all numbers greater than their indexes... 
            //until a number smaller than the index is found, then it stops searching
            
            //Where would return all numbers greater than their index position, 
            //TakeWhile selects until expression is false

            foreach (var number in results)
            {
                Console.WriteLine(number);
            }
        }

        public void Ex17()
        {
            var numbersC = DataLoader.NumbersC;
            var results = numbersC.Where(n => n%3 == 0);

            foreach (var number in results)
            {
                Console.WriteLine(number);
            }

        }

        public void Ex18()
        {
            var products = DataLoader.LoadProducts();
            var results = products.OrderBy(p => p.ProductName);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        public void Ex19()
        {
            var products = DataLoader.LoadProducts();
            var results = products.OrderByDescending(p => p.UnitsInStock);

            foreach (var product in results)
            {
                Console.WriteLine(product.UnitsInStock + ", " + product.ProductName);
            }

        }

        public void Ex20()
        {
            var products = DataLoader.LoadProducts();
            var results = products.OrderBy(p => p.Category)
                .ThenByDescending(p => p.UnitPrice)
                .Select(p => new {p.Category, p.UnitPrice, p.ProductName});

            foreach (var product in results)
                Console.WriteLine(product);
        }

        public void Ex21()
        {
            var numbersC = DataLoader.NumbersC;
            var results = numbersC.Reverse();

            foreach (var number in results)
                Console.WriteLine(number);            
        }

        public void Ex22()
        {
            var numbersC = DataLoader.NumbersC;
            var results =
                numbersC
                    .GroupBy(n => n%5, n => new {mod5 = n%5, number = n})
                    .Select(number => number);



            foreach (var number in results)
            {
                Console.WriteLine();
                foreach (var n in number)
                Console.WriteLine(n); //.Key + ", " + n);
            }
        }

        public void Ex23()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                select p.Category;

            foreach (var product in results)
            {
                Console.WriteLine(product);
            }
        }

        public void Ex24() //Group customer orders by year, then by month.
        {
            var customers = DataLoader.LoadCustomers();

            var results = customers.Select(c => new
            {
                c.CompanyName,
                yearOrder = from o in c.Orders
                    group o by o.OrderDate.Year
                    into yo
                    select new
                    {
                        year = yo.Key,
                        MonthGroup = from o in yo
                            group o by o.OrderDate.Month
                            into mo
                            select new
                            {
                                Month = mo.Key,
                                Orders = from o in mo
                                    group o by o.OrderID
                                    into oid
                                    select new
                                    {
                                        orderId = oid.Key
                                    }
                            }
                    }
            });

            foreach (var r in results) //our ienumerable
            {
                Console.WriteLine("{0}", r.CompanyName);
                foreach (var y in r.yearOrder)
                {
                    Console.WriteLine("\t{0}", y.year);
                    foreach (var m in y.MonthGroup)
                    {
                        Console.WriteLine("\t\t{0}", m.Month);
                        foreach (var o in m.Orders)
                        {
                            Console.WriteLine("\t\t\t{0}", o.orderId);
                        }
                    }
                }
            }

        }

        public void Ex25()
        {
            var products = DataLoader.LoadProducts();
            var results = products.OrderBy(p => p.Category).Select(p => p.Category).Distinct();

            foreach (var product in results)
            {
                Console.WriteLine(product);
            }
        }

        public void Ex26()
        {
            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;

            var results = numbersA.Union(numbersB);

            foreach (var n in results)
                Console.WriteLine(n);
        }

        public void Ex27()
        {
            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;

            var results = numbersA.Intersect(numbersB);

            foreach (var n in results)
                Console.WriteLine(n);
        }

        public void Ex28()
        {
            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;

            var results = numbersA.Except(numbersB);

           foreach (var n in results)
                Console.WriteLine(n);
        }

        public void Ex29()
        {
            var products = DataLoader.LoadProducts();
            var results = products.Where(p => p.ProductID == 12)
                .Select(p => new {p.ProductID, p.ProductName}).First();

            Console.WriteLine(results);
        }

        public void Ex30()
        {
            var products = DataLoader.LoadProducts();
            var results = products.Where(p => p.ProductID == 789).Select(p => p.ProductID);

            foreach (var product in results)
                Console.WriteLine(product);           
        }

        public void Ex31()
        {
         var products = DataLoader.LoadProducts();
         var results = products.Where(p => p.UnitsInStock == 0).Select(p => p.Category).Distinct();

            foreach (var category in results)
                Console.WriteLine(category);   
        }

        public void Ex32()
        {
            var numbersB = DataLoader.NumbersB;
            var results = numbersB.All(n => n < 9);

            Console.WriteLine(results);   
        }

        public void Ex33()
        {
            //33. Get a grouped a list of products only for categories that have all of their products in stock.
            //only return products that are a part of categories with everything in stock
            
            var products = DataLoader.LoadProducts();
            var results = products.GroupBy(p => p.Category).Where(p2 => p2.All(p => p.UnitsInStock > 0));

                   foreach (var result in results)
                   {
                       Console.WriteLine(result.Key);
                       foreach (var product in result)
                           Console.WriteLine("\t{0} {1}", product.ProductName, product.UnitsInStock);
                   }
        }

        public void Ex34()
        {
            var numbersA = DataLoader.NumbersA;
            var results = numbersA.Where(p => p % 2 == 1).Count();

            Console.WriteLine(results);
        }

        public void Ex35()
        {
            var customers = DataLoader.LoadCustomers();
            var results = customers.Select(c => new {c.CustomerID, Orders = c.Orders.Length});
            
            foreach (var c in results)
            Console.WriteLine(c);
        }

        public void Ex36()
        { //Display a list of categories and the count of their products.
            var products = DataLoader.LoadProducts();

            var results = products.GroupBy(p => p.Category).Select(p => new {category = p.Key, totalProducts = 
                (p.Select(p2 => p2.ProductName).Count())});

            foreach (var c in results)
            Console.WriteLine(c);
        }

        public void Ex37()
        {
            var products = DataLoader.LoadProducts();

            var results = products.GroupBy(p => p.Category).Select(p => new {category = p.Key, totalUnits = 
                (p.Sum(p2 => p2.UnitsInStock))});

            foreach (var c in results)
            Console.WriteLine(c);
        }

        public void Ex38()
        {
            var products = DataLoader.LoadProducts();

            var results = products.GroupBy(p => p.Category).Select(p => new {category = p.Key, lowestPrice = 
                (p.OrderBy(p2 => p2.UnitPrice).Select(p2 => p2.UnitPrice).First())});

            foreach (var c in results)
            Console.WriteLine(c);
        }

        public void Ex39()
        {
            var products = DataLoader.LoadProducts();

            var results = products.GroupBy(p => p.Category).Select(p => new {category = p.Key, highestPrice = 
                (p.OrderByDescending(p2 => p2.UnitPrice).Select(p2 => p2.UnitPrice).First())});

            foreach (var c in results)
            Console.WriteLine(c);
        }

        public void Ex40()
        {
            var products = DataLoader.LoadProducts();

            var results = products.GroupBy(p => p.Category).Select(p => new
            { category = p.Key,
                avgPrice = 
                (p.Select(p2 => p2.UnitPrice).Average())});

            foreach (var c in results)
            Console.WriteLine(c);
        }
    }
}
