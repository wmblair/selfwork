﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;

namespace WarmupLabStrings
{
    public class Strings
    {
        //SayHi
        public string SayHi(string name)
        {
            return string.Format("Hello {0}!", name);
        }

        public string Abba(string a, string b)
        {
            return string.Format("{0}{1}{1}{0}", a, b);
        }

        public string MakeTags(string tag, string content)
        {
            return string.Format("<{0}>{1}</{0}>", tag, content);
        }

        public string InsertWord(string container, string word)
        {
            string sub = container.Insert(container.Length/2, word);
            return sub;
        }

        public string MultipleEndings(string str)
        {
            if (str.Length <= 2)
                return string.Format("{0}{0}{0}", str);

            string sub = str.Substring(str.Length - 2);
            return string.Format("{0}{0}{0}", sub);
        }

        public string FirstHalf(string str)
        {
            string sub = str.Remove(str.Length/2);
            return sub;
        }

        public string TrimOne(string str)
        {
            string sub = str.Substring(1, str.Length - 2);
            return sub;
        }

        public string LongInMiddle(string a, string b)
        {
            if (a.Length > b.Length)
                return string.Format("{0}{1}{0}", b, a);

            return string.Format("{0}{1}{0}", a, b);
        }

        public string RotateLeft2(string str)
        {
            string sub = str.Substring(0, 2);
            str = str.Remove(0, 2);
            str += sub;
            return str;
        }

        public string RotateRight2(string str)
        {
            string sub = str.Substring(str.Length-2, 2);
            str = str.Remove(str.Length - 2, 2);
            sub += str;
            return sub;
        }

        public string TakeOne(string str, bool fromFront)
        {
            if (fromFront)
                return str[0].ToString();

            return str[str.Length - 1].ToString();
        }

        public string MiddleTwo(string str)
        {
            string sub = str.Substring((str.Length/2) - 1, 2);
            return sub;
        }

        public bool EndsWithLy(string str)
        {
            string sub = str.Substring(str.Length - 2, 2);

            if (sub.Equals("ly"))
                return true;

            return false;
        }

        public string FrontAndBack(string str, int n)
        {
            string sub = "";
            sub = str.Substring(0, n);
            sub += str.Substring(str.Length-n, n);
            return sub;
        }

        public string TakeTwoFromPosition(string str, int n)
        {
            string sub = "";
            if (n + 1 < str.Length)
            {
                sub = str.Substring(n, 2);
            }
            if (sub.Length < 2)
            {
                sub = str.Substring(0, 2);
                return sub;
            }
            return sub;
        }

        public bool HasBad(string str)
        {
            if (str.Length == 3)
            {
                if (str.Equals("bad"))
                {
                    return true;
                }
            }
            else if (str.Length >= 4)
            {
                if (str.Substring(0, 4).Contains("bad"))
                {
                    return true;
                }
            }
            return false;
        }

        public string AtFirst(string str)
        {
            StringBuilder sb = new StringBuilder();

            if (str.Length == 1)
            {
                sb.Append(str);
                sb.Append("@");
            }
            else
            {
                sb.Append(str[0]);
                sb.Append(str[1]);
            }
            return sb.ToString();
        }

        public string LastChars(string a, string b)
        {
            StringBuilder sb = new StringBuilder();

            if (b.Length > 0)
            {
                sb.Append(a[0]);
                sb.Append(b[b.Length - 1]);
                return sb.ToString();
            }
            sb.Append(a[0]);
            sb.Append("@");

            return sb.ToString();
        }

        public string ConCat(string a, string b)
        {
            if (a[a.Length - 1].Equals(b[0]))
            {
                return string.Concat(a.Remove(a.Length - 1), b);
            }
            return string.Concat(a, b);
        }

        public string SwapLast(string str)
        {
            string sub = str.Substring(0, str.Length-2);
            string sub2 = str.Substring(str.Length-2, 2);
            return string.Concat(sub, string.Concat(sub2[1], sub2[0]));
        }

        public bool FrontAgain(string str)
        {
            string sub = string.Concat(str[0], str[1]);
            string sub2 = string.Concat(str[str.Length - 2], str[str.Length - 1]);

            if (sub.Equals(sub2))
            {
                return true;
            }
            return false;
        }

        public string MinCat(string a, string b)
        {
            //2, 1
            if (a.Length > b.Length)
            {
                string sub = a.Substring(a.Length-b.Length, b.Length);
                return string.Concat(sub, b);
            }
            if (b.Length > a.Length)
            {
                string sub = b.Substring(b.Length-a.Length, a.Length);
                return string.Concat(a, sub);
            }
            return string.Concat(a,b);
        }

        public string TweakFront(string str)
        {
            string sub = str.Substring(2, str.Length - 2);

            if (str[0] =='a' && str[1] != 'b')
            {                
                sub = sub.Insert(0,"a");
                return sub;
            }
            if (str[1] == 'b' && str[0] != 'a')
            {
                sub = sub.Insert(0,"b");
                return sub;
            }
            if (str[0] == 'a' && str[1] == 'b')
            {
                sub = sub.Insert(0, "b");
                sub = sub.Insert(0, "a");               
                return sub;
            }
            return str.Substring(2, str.Length - 2);            
        }

        public string StripX(string str)
        {
            if (str[0] == 'x' && str[str.Length - 1] == 'x')
            {
                string sub = str.Remove(0, 1);
                sub = sub.Remove(sub.Length-1);
                return sub;
            }
            if (str[0] == 'x')
            {
                return str.Remove(0, 1);
            }
            if (str[str.Length - 1] == 'x')
            {
                return str.Remove(str.Length-1);
            }
            return str;
        }

    }
}

