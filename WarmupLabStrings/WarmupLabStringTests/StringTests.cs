﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WarmupLabStrings;

namespace WarmupLabStringTests
{
    [TestFixture]
    public class StringsTests
    {
        private Strings _strings;

        [SetUp]
        public void BeforeEachTest()
        {
            _strings = new Strings();
        }

        [TestCase("Bob", "Hello Bob!")]
        [TestCase("Alice", "Hello Alice!")]
        [TestCase("X", "Hello X!")]
        public void SayHiReturnsExpected(string input, string expected)
        {
            string result = _strings.SayHi(input);
            Assert.AreEqual(expected, result);
        }
        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("What", "Up", "WhatUpUpWhat")]
        public void AbbaReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.Abba(a, b);
            Assert.AreEqual(expected, result);
        }

        [TestCase("i", "Hello", "<i>Hello</i>")]
        public void MakeTagsReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.MakeTags(a, b);
            Assert.AreEqual(expected, result);
        }
        [TestCase("[[]]", "Hello", "[[Hello]]")]
        public void InsertWordReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.InsertWord(a, b);
            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", "lololo")]
        public void MultipleEndingsReturnsExpected(string input, string expected)
        {
            string result = _strings.MultipleEndings(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("HelloWatup","Hello")]
        public void FirstHalfReturnsExpected(string input, string expected)
        {
            string result = _strings.FirstHalf(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("watup", "atu")]
        public void TrimOneReturnsExpected(string input, string expected)
        {
            string result = _strings.TrimOne(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("yo", "watup", "yowatupyo")]
        [TestCase("supdog", "hey", "heysupdoghey")]
        public void LongInMiddleReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.LongInMiddle(a,b);
            Assert.AreEqual(expected, result);
        }

        [TestCase("hello", "llohe")]
        [TestCase("book", "okbo")]
        public void RotateLeft2ReturnsExpected(string input, string expected)
        {
            string result = _strings.RotateLeft2(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("hello", "lohel")]
        [TestCase("firewood", "odfirewo")]
        public void RotateRight2ReturnsExpected(string input, string expected)
        {
            string result = _strings.RotateRight2(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("book", false, "k")]
        public void TakeOneReturnsExpected(string input, bool fromFront, string expected)
        {
            string result = _strings.TakeOne(input, fromFront);
            Assert.AreEqual(expected, result);
        }

        [TestCase("book", "oo")]
        [TestCase("testings", "ti")]
        public void MiddleTwoReturnsExpected(string input, string expected)
        {
            string result = _strings.MiddleTwo(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("feedly", true)]
        [TestCase("heehee", false)]
        [TestCase("cloudy", false)]
        public void EndsWithLyReturnsExpected(string input, bool expected)
        {
            bool result = _strings.EndsWithLy(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("supercalifragilisticexpialadocious", 5, "supercious")]
        public void FrontAndBackReturnsExpected(string input, int n, string expected)
        {
            string result = _strings.FrontAndBack(input, n);
            Assert.AreEqual(expected, result);
        }

        [TestCase("wednesday", 4, "es")]
        [TestCase("thursday", 7, "th")]
        public void TakeTwoFromPositionReturnExpected(string input, int n, string expected)
        {
            string result = _strings.TakeTwoFromPosition(input, n);
            Assert.AreEqual(expected, result);
        }

        [TestCase("bad", true)]
        [TestCase("xbadxxxxx", true)]
        [TestCase("xxbadxxx", false)]
        public void HasBadReturnsExpected(string input, bool expected)
        {
            bool result = _strings.HasBad(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("feedly", "fe")]
        [TestCase("haahaaha", "ha")]
        [TestCase("c", "c@")]
        public void AtFirstReturnsExpected(string input, string expected)
        {
            string result = _strings.AtFirst(input);
            Assert.AreEqual(expected, result);
        }
        [TestCase("feed", "funny", "fy")]
        [TestCase("hey", "ya", "ha")]
        [TestCase("c", "", "c@")]
        public void LastCharsReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.LastChars(a,b);
            Assert.AreEqual(expected, result);
        }

        [TestCase("tel", "ling", "teling")]
        [TestCase("hey", "you", "heyou")]
        [TestCase("crab", "cakes", "crabcakes")]
        public void ConCatReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.ConCat(a, b);
            Assert.AreEqual(expected, result);
        }

        [TestCase("crab", "crba")]
        [TestCase("wellington", "wellingtno")]
        public void SwapLastReturnsExpected(string input, string expected)
        {
            string result = _strings.SwapLast(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("rara", true)]
        [TestCase("samosa", true)]
        [TestCase("rock", false)]
        public void FrontAgainReturnsExpected(string input, bool expected)
        {
            bool result = _strings.FrontAgain(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("Hello", "Hi", "loHi")]
        [TestCase("Hello", "java", "ellojava")]
        [TestCase("java", "Hello", "javaello")]
        [TestCase("Hi", "", "")]
        public void MinCatReturnsExpected(string a, string b, string expected)
        {
            string result = _strings.MinCat(a, b);
            Assert.AreEqual(expected, result);
        }

        [TestCase("morning", "rning")]
        [TestCase("afternoon", "aternoon")]
        [TestCase("abacus", "abacus")]
        [TestCase("bottle", "ttle")]
        [TestCase("obsolete", "bsolete")]
        public void TweakFrontReturnsExpected(string input, string expected)
        {
            string result = _strings.TweakFront(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("stripx", "strip")]
        [TestCase("xylophone", "ylophone")]
        [TestCase("xyzx", "yz")]
        [TestCase("xxyzxx", "xyzx")]
        public void StripXReturnsExpected(string input, string expected)
        {
            string result = _strings.StripX(input);
            Assert.AreEqual(expected, result);
        }
    }




}
