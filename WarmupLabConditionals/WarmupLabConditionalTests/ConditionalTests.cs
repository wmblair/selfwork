﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WarmupLabConditionals;

namespace WarmupLabConditionalTests
{
    [TestFixture]
    public class ConditionalTests
    {
        private Conditionals _conditional;

        [SetUp]
        public void RunBeforeTests()
        {
            _conditional = new Conditionals();
        }

        [TestCase(true, false, false)]
        [TestCase(false, true, false)]
        [TestCase(true, true, true)]
        [TestCase(false, false, true)]
        public void AreWeInTroubleReturnsExpected(bool a, bool b, bool expected)
        {
            bool result = _conditional.AreWeInTrouble(a, b);
            Assert.AreEqual(result, expected);
        }

        [TestCase(true, false, false)]
        [TestCase(false, false, true)]
        [TestCase(true, true, true)]
        [TestCase(false, true, true)]
        public void CanSleepInReturnsExpected(bool a, bool b, bool expected)
        {
            bool result = _conditional.CanSleepIn(a, b);
            Assert.AreEqual(result, expected);
        }

        [TestCase(2, 2, 8)]
        [TestCase(5, 5, 20)]
        [TestCase(5, 4, 9)]
        public void SumDoubleReturnsExpected(int a, int b, int expected)
        {
            int result = _conditional.SumDouble(a, b);
            Assert.AreEqual(result, expected);
        }

        [TestCase(23, 4)]
        [TestCase(10, 11)]
        [TestCase(21, 0)]
        public void Diff21ReturnsExpected(int n, int expected) {
            int result = _conditional.Diff21(n);
            Assert.AreEqual(result, expected);
        }

        [TestCase(true, 21, true)]
        [TestCase(true, 2, true)]
        [TestCase(true, 11, false)]
        [TestCase(false, 22, false)]
        [TestCase(false, 15, false)]
        public void ParrotTroubleReturnsExpected(bool isTalking, int hour, bool expected) {
            bool result = _conditional.ParrotTrouble(isTalking, hour);
            Assert.AreEqual(result, expected);
        }

        [TestCase(2, 8, true)]
        [TestCase(10, 0, true)]
        [TestCase(5, 10, true)]
        [TestCase(4, 5, false)]
        [TestCase(16, 7, false)]
        [TestCase(11, -1, true)]
        public void Makes10ReturnsExpected(int a, int b, bool expected) {
            bool result = _conditional.Makes10(a,b);
            Assert.AreEqual(result, expected);
        }

        [TestCase(103, true)]
        [TestCase(111, false)]
        [TestCase(189, false)]
        [TestCase(195, true)]
        [TestCase(210, true)]
        [TestCase(92, true)]
        public void NearHundredReturnsExpected(int n, bool expected) {
            bool result = _conditional.NearHundred(n);
            Assert.AreEqual(result, expected);
        }

        [TestCase(2, 5, false, false)]
        [TestCase(2, -5, false, true)]
        [TestCase(-2, 5, false, true)]
        [TestCase(-2, -5, false, false)]
        public void PosNegReturnsExpected(int a, int b, bool negative, bool expected)
        {
            bool result = _conditional.PosNeg(a, b, negative);
            Assert.AreEqual(result, expected);
        }

        [TestCase("candy", "not candy")]
        [TestCase("x", "not x")]
        [TestCase("not", "not")]
        [TestCase("notbad", "notbad")]
        [TestCase("not bad", "not bad")]
        public void NotStringReturnsExpected(string input, string expected) {
            string result = _conditional.NotString(input);
            Assert.AreEqual(result, expected);
        }

        [TestCase("segway", 2, "seway")]
        [TestCase("segway", 0, "egway")]
        [TestCase("segway", 5, "segwa")]
        public void MissingCharReturnsExpected(string input, int n, string expected) {
            string result = _conditional.MissingChar(input, n);
            Assert.AreEqual(result, expected);
        }

        [TestCase("frontback", "krontbacf")]
        public void FrontBackReturnsExpected(string input, string expected)
        {
            string result = _conditional.FrontBack(input);
            Assert.AreEqual(result, expected);
        }

        [TestCase("microsoft", "micmicmic")]
        [TestCase("Chocolate", "ChoChoCho")]
        [TestCase("at", "atatat")]
        public void Front3ReturnsExpected(string input, string expected) {
            string result = _conditional.Front3(input);
            Assert.AreEqual(result, expected);
        }

        [TestCase("what", "twhatt")]
        [TestCase("good", "dgoodd")]
        public void BackAroundReturnsExpected(string input, string expected)
        {
            string result = _conditional.BackAround(input);
            Assert.AreEqual(result, expected);
        }

        [TestCase(3, true)]
        [TestCase(5, true)]
        [TestCase(8, false)]
        [TestCase(11, false)]
        [TestCase(15, true)]
        public void Multiple3Or5ReturnsExpected(int number, bool expected) {
            bool result = _conditional.Multiple3Or5(number);
            Assert.AreEqual(result, expected);
        }

        [TestCase("hi", true)]
        [TestCase("hi there", true)]
        [TestCase("high up", false)]
        public void StartHiReturnsExpected(string input, bool expected) {
            bool result = _conditional.StartHi(input);
            Assert.AreEqual(result, expected);
        }

        [TestCase(120, -1, true)]
        [TestCase(-3, 113, true)]
        [TestCase(99, -5, false)]
        public void IcyHotReturnsExpected(int a, int b, bool expected) {
            bool result = _conditional.IcyHot(a,b);
            Assert.AreEqual(result, expected);
        }

        [TestCase(12, 99, true)]
        [TestCase(21, 12, true)]
        [TestCase(8, 99, false)]
        public void Between10And20ReturnsExpected(int a, int b, bool expected)
        {
            bool result = _conditional.Between10And20(a,b);
            Assert.AreEqual(result, expected);
        }

        [TestCase(13, 20, 10, true)]
        [TestCase(20, 19, 10, true)]
        [TestCase(20, 10, 12, false)]
        public void HasTeenReturnsExpected(int a, int b, int c, bool expected) {
            bool result = _conditional.HasTeen(a,b,c);
            Assert.AreEqual(result, expected);
        }

        [TestCase(13,99,true)]
        [TestCase(21,19,true)]
        [TestCase(13,13,false)]
        public void SoAloneReturnsExpected(int a, int b, bool expected) {
            bool result = _conditional.SoAlone(a,b);
            Assert.AreEqual(result, expected);
        }

        [TestCase("adelbc","abc")]
        [TestCase("adelHello","aHello")]
        [TestCase("adedbc","adedbc")]
        public void RemoveDelReturnsExpected(string input, string expected) {
            string result = _conditional.RemoveDel(input);
            Assert.AreEqual(result, expected);
        }

        [TestCase("ix center",false)]
        [TestCase("pix center",true)]
        [TestCase("mixcenter",true)]
        public void IxStartReturnsExpectedt(string input, bool expected) {
            bool result = _conditional.IxStart(input);
            Assert.AreEqual(result, expected);
        }

        [TestCase("ozymandias","oz")]
        [TestCase("bzoo","z")]
        [TestCase("oxx","o")]
        [TestCase("bump","bu")]
        public void StartOzReturnsExpected(string input, string expected) {
            string result = _conditional.StartOz(input);
            Assert.AreEqual(result, expected);
        }

        [TestCase(1,2,3,3)]
        [TestCase(1,4,3,4)]
        [TestCase(5,1,1,5)]
        [TestCase(4,1,4,4)]
        public void MaxReturnsExpected(int a, int b, int c, int expected) {
            int result = _conditional.Max(a,b,c);
            Assert.AreEqual(result, expected);
        }

        [TestCase(8,13,8)]
        [TestCase(13,8,8)]
        [TestCase(13,7,0)]
        public void CloserReturnsExpected(int a, int b, int expected) {
            int result = _conditional.Closer(a,b);
            Assert.AreEqual(result, expected);
        }

        [TestCase("Hello",true)]
        [TestCase("Heelle",true)]
        [TestCase("Heelele",false)]
        public void GotEReturnsExpected(string input, bool expected) {
            bool result = _conditional.GotE(input);
            Assert.AreEqual(result, expected);
        }

        [TestCase("Hello","HeLLO")]
        [TestCase("it","IT")]
        public void EndUpReturnsExpected(string input, string expected)
        {
            string result = _conditional.EndUp(input);
            Assert.AreEqual(result, expected);
        }

        [TestCase("Miracle",2,"Mrce")]
        [TestCase("abcdefg",3,"adg")]
        public void EveryNthReturnsExpected(string input, int n, string expected)
        {
            string result = _conditional.EveryNth(input, n);
            Assert.AreEqual(result, expected);
        }
    }
}
