﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace WarmupLabConditionals
{
    public class Conditionals
    {
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            if (aSmile == bSmile)
                return true;
            return false;
        }

        public bool CanSleepIn(bool isWeekday, bool isVacation)
        {
            if (isWeekday)
            {
                if (isVacation)
                    return true;
                return false;
            }
            return true;
        }

        public int SumDouble(int a, int b)
        {
            if (a == b)
                return (a + b)*2;
            return a + b;
        }

        public int Diff21(int n)
        {
            if (Math.Abs(n) <= 21)
                return 21 - n;
            return (n - 21) * 2;
        }

        public bool ParrotTrouble(bool isTalking, int hour) {
            if (hour >= 0 && hour <= 23)
            {
                if (hour <= 6 || hour >= 19)
                {
                    if(isTalking)
                        return true;
                    return false;
                }
                return false;
            }
            return false;
        }

        public bool Makes10(int a, int b)
        {
            if (a + b != 10)
            {
                if (a == 10 || b == 10)
                    return true;
                return false;
            }
            return true;
        }

        public bool NearHundred(int n)
        {
            if (Math.Abs(100 - n) <= 10)
                return true;
            if (Math.Abs(200 - n) <= 10)
                return true;
            return false;
        }

        public bool PosNeg(int a, int b, bool negative)
        {
            if (!negative)
            {
                if (Math.Abs(a) + Math.Abs(b) != Math.Abs(a + b))
                    return true;
                return false;
            }
            if(Math.Abs(a) != a && Math.Abs(b) != b)
                return true;
            return false;
        }

        public string NotString(string s)
        {
            if (s.Length >= 3)
            {
                if (s.Substring(0, 3).ToLower() == "not")
                    return s;
            }
            s = s.Insert(0, "not ");
            return s;
        }

        public string MissingChar(string str, int n)
        {
            str = str.Remove(n, 1);
            return str;
        }

        public string FrontBack(string str)
        {           
            StringBuilder sb = new StringBuilder();
            sb.Append(str);

            char temp = sb[0];
            sb[0] = sb[sb.Length - 1];
            sb[sb.Length - 1] = temp;

            return sb.ToString();
        }

        public string Front3(string str) {
            if (str.Length >= 3)
            {
                str = str.Substring(0,3);
                return str + str + str;
            }
            return str + str + str;
        }

        public string BackAround(string str)
        {
            string lastChar = str[str.Length-1].ToString(); //str[str.Length - 1].ToString();

            if (str.Length >= 1)
            {
                str = str.Insert(str.Length-1, lastChar);
                str = str.Insert(0, lastChar);
                return str;
            }
            return str;
        }

        public bool Multiple3Or5(int number)
        {
            if (number % 3 == 0 || number % 5 == 0)
                return true;
            return false;
        }

        public bool StartHi(string str)
        {
            if (str == "hi" || str.Substring(0, 3) == "hi ")
                return true;
            return false;
        }

        public bool IcyHot(int temp1, int temp2) {
            if ((temp1 < 0 || temp2 < 0) && (temp1 > 100 || temp2 > 100))
                return true;
            return false;
        }

        public bool Between10And20(int a, int b)
        {
            if ((a >= 10 && a <= 20) || (b >= 10 && b <= 20))
                return true;
            return false;
        }

        public bool HasTeen(int a, int b, int c)
        {
            if (a >= 13 && a <= 19 || b >= 13 && b <= 19 || c >= 13 && c <= 19)
                return true;
            return false;
        }

        public bool SoAlone(int a, int b)
        {
            //if (a >= 13 && a <= 19 && Math.Abs(a - b) <= 6)
            //    return false;
           // if (a >= 13 && a <= 19 || Math.Abs(a - b) <= 6)
            //    return true;
           // return false;

            if (a >= 13 && a <= 19)
            {
                if (b >= 13 && b <= 19)
                    return false;
                return true;
            }
            if (b >= 13 && b <= 19)
            {
                if (a >= 13 && a <= 19)
                    return false;
                return true;
            }
            return false;

        }

        public string RemoveDel(string str) {
            if (str.Substring(1,3) == "del")
                str = str.Remove(1, 3);
            return str;
        }

        public bool IxStart(string str)
        {
            if (str.Substring(1, 2) == "ix")
                return true;
            return false;
        }

        public string StartOz(string str)
        {
            if (str.Substring(0, 2) == "oz")
                return "oz";
            if (str[0] == 'o')
                return "o";
            if (str[1] == 'z')
                return "z";
            return str.Substring(0, 2);
        }

        public int Max(int a, int b, int c)
        {
            if (a > b && a > c)
                return a;
            if (b > a && b > c)
                return b;
            return c;
        }

        public int Closer(int a, int b)
        {
            if (Math.Abs(10 - a) < Math.Abs(10 - b))
                return a;
            if (Math.Abs(10 - b) < Math.Abs(10 - a))
                return b;
            return 0;
        }

        public bool GotE(string str)
        {
            int count = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == 'e')
                    ++count;
                if (count >= 4)
                    return false;
            }
            if (count > 0 && count <= 3)
                return true;
            return false;
        }

        public string EndUp(string str)
        {
            if (str.Length < 3)
            {
                str = str.ToUpper();
                return str;
            }
            string sub = str.Substring(str.Length-3, 3);
            str = str.Remove(str.Length-3, 3);
            str = str.Insert(str.Length, sub.ToUpper());
            return str;
        }

        public string EveryNth(string str, int n)
        {
            string sub = "";
            for (int i = 0; i < str.Length; i += n)
            {
                sub += str[i];
            }
            return sub;
        }
    }
}
