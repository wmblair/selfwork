﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipCalculator
{
    class TipCalculator
    {
        static void Main(string[] args)
        {
            double amount = 0, tip = 0;

            amount = PromptUserForDollarAmount(amount);
            tip = PromptUserForTipPercentage(tip);
            double result = CalculateTip(amount, tip);

            Console.WriteLine("Total: $" + result);
            Console.ReadLine();
        }

        static double CalculateTip(double value, double theTip)
        {
            return value + theTip;
        }

        static double PromptUserForDollarAmount(double theAmount)
        {
            string strAmount = "";
            Console.Write("Amount: $");
            do
                strAmount = Console.ReadLine();
            while (!double.TryParse(strAmount, out theAmount));
            return theAmount;
        }

        static double PromptUserForTipPercentage(double thePercentage)
        {
            string strPercentage = "";
            int intPerc = 0;
            Console.Write("Tip: $");
            strPercentage = Console.ReadLine();

            if (int.TryParse(strPercentage, out intPerc))
            {
                thePercentage /= 100;
                return thePercentage;
            }                   
                while (!double.TryParse(strPercentage, out thePercentage));      
                strPercentage = Console.ReadLine();     
            return thePercentage;
        }
    }
}
